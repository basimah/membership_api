"""Add meeting invitations table

Revision ID: 691664236336
Revises: 57f9bc49bcd7
Create Date: 2020-05-17 17:19:35.969059

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
from membership.database.types import UTCDateTime

revision = "691664236336"
down_revision = "57f9bc49bcd7"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "meeting_invitation",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", UTCDateTime(), nullable=True),
        sa.Column("meeting_id", sa.Integer(), nullable=False),
        sa.Column("member_id", sa.Integer(), nullable=False),
        sa.Column(
            "token",
            sa.String(length=128, collation="utf8mb4_unicode_520_ci"),
            nullable=True,
        ),
        sa.Column(
            "status",
            sa.Enum(
                "NO_RESPONSE", "ACCEPTED", "DECLINED", name="meetinginvitationstatus"
            ),
            nullable=True,
        ),
        sa.ForeignKeyConstraint(["meeting_id"], ["meetings.id"]),
        sa.ForeignKeyConstraint(["member_id"], ["members.id"]),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("id"),
        sa.UniqueConstraint("meeting_id", "member_id"),
        sa.UniqueConstraint("token"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("meeting_invitation")
    # ### end Alembic commands ###
