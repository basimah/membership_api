from typing import Optional

from config.lib import from_env, throw

USE_AUTH: bool = from_env.get_bool('USE_AUTH', True)

# the user to authenticate as if external auth is off (for testing only)
NO_AUTH_EMAIL: Optional[str] = from_env.get_str('NO_AUTH_EMAIL', None if USE_AUTH else throw)

# the client id and secret for verifying users token
JWT_SECRET: Optional[str] = from_env.get_str('JWT_SECRET', throw if USE_AUTH else None)
JWT_CLIENT_ID: Optional[str] = from_env.get_str('JWT_CLIENT_ID', throw if USE_AUTH else None)
AUTH_URL: str = from_env.get_str('AUTH_URL', 'https://dsasf.auth0.com/')
AUTH_CONNECTION: str = from_env.get_str('AUTH_CONNECTION', 'DSASF-Dev')

# the client ID and secret for the non-interactive auth client (for creating users in auth0)
ADMIN_CLIENT_ID: Optional[str] = from_env.get_str('ADMIN_CLIENT_ID', throw if USE_AUTH else None)
ADMIN_CLIENT_SECRET: Optional[str] = from_env.get_str(
    'ADMIN_CLIENT_SECRET',
    throw if USE_AUTH else None
)

EMAIL_TOPICS_ADD_MEMBER_TOKEN: str = (
    from_env.get_str(
        'EMAIL_TOPICS_ADD_MEMBER_TOKEN',
        throw if USE_AUTH else None
    )
)

DISABLE_CREATE_AUTH0_USER: bool = from_env.get_bool('DISABLE_CREATE_AUTH0_USER', False)
