default: docker-api

# DEPRECATED
dev: local
	echo "'make dev' is deprecated. Use 'make local' instead."

# Run all tests in the container (same as CI)
test: lint
	docker-compose build test
	docker run dsasanfrancisco/membership_test

# Run quick unit tests without building the image
unittest:
	docker-compose run test -m 'not slow'

# Run unit tests and report coverage without building the image
unittest-cov:
	docker-compose run test --cov=membership --cov=jobs --cov-report=html

# Run the mypy type checker
typecheck:
	poetry run mypy -p membership -p jobs

security:
	bandit -c bandit.yml -q -r membership jobs

security-all:
	bandit -c bandit.yml -r membership jobs

# Run code formatter
fmt:
	pre-commit run -a

# Run python linter
lint:
	docker-compose run --entrypoint='flake8' test

# Upgrade the schema in the database to the latest version
migrate: load
	docker-compose up migrate

# Downgrade the schema in the database by one version
migrate-down: load
	docker-compose run migrate alembic downgrade -1

# Generate a migration with the given message variable
migration:
	[ "$(MESSAGE)" != "" ] \
		&& docker-compose run migrate alembic revision --autogenerate -m "$(MESSAGE)" \
		|| echo "Usage: make migration MESSAGE='...'"

# Load all required infrastructure in docker (databases, caches, etc)
load:
	docker-compose up -d db

# Start with poetry
local: load
	poetry install
	poetry run python flask_app.py

# Start the api with docker
docker-api:
	docker-compose up -d
	docker-compose logs -f

# DEPRECATED
docker:
	echo "Make target 'docker' is deprecated. Use 'docker-api' instead."

# Start with docker, including web ui
docker-all:
	docker-compose \
		-f docker-compose.yml -f docker-compose.override.yml -f docker-compose.ui.yml \
		up -d
	docker-compose logs -f

# DEPRECATED
all:
	echo "Make target 'all' is deprecated. Use 'docker-all' instead."

# Stop all docker containers, including web ui
stop:
	docker-compose \
		-f docker-compose.yml -f docker-compose.override.yml -f docker-compose.ui.yml \
		stop

# Build with docker
build:
	docker-compose build

# Clean up local files and one-time run containers
clean:
	find . | \
	grep -E "(__pycache__|\.pyc$$|\.sqlite$$)" | \
	xargs rm -rf && \
	docker-compose rm -f

# Remove all local code, stopped containers, and databases for this project
purge: clean
	rm -rf mnt
	docker volume prune -f

# Kill the running applications (docker-only)
kill:
	docker-compose kill

# In case you really need to clean house, but don't want to waste time spelling out all the commands
stalin: kill purge
	docker system prune

# There is a directory named docker
.PHONY: docker
