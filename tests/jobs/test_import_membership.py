from io import StringIO
from typing import Dict
from unittest import TestCase

from overrides import overrides

from jobs.import_membership import (
    ForceConfirmAction,
    ImportNationalMembership,
    NationalMembershipKeys,
)
from membership.database.base import Session, engine, metadata
from membership.database.models import (
    Member,
    NationalMembershipData,
    Role,
    AdditionalEmailAddress,
)
from tests.io_utils import devnull


class TestDB(TestCase):
    @classmethod
    @overrides
    def setUp(cls):
        metadata.create_all(engine)

    @classmethod
    @overrides
    def tearDown(cls):
        metadata.drop_all(engine)


class TestImportNationalMembership(TestDB):
    job = ImportNationalMembership()
    keys = NationalMembershipKeys
    default_import_args = dict(out=devnull, confirm_action=ForceConfirmAction,)

    # Helper methods

    def _unique_row(self, row: Dict[str, str]):
        if not getattr(self, "__idx", None):
            self.__idx = 0
        self.__idx += 1
        unique = {
            self.keys.AK_ID: str(self.__idx),
            self.keys.FIRST_NAME: "Joe",
            self.keys.LAST_NAME: "Schmoe",
            self.keys.CITY: "San Francisco",
            self.keys.COUNTRY: "usa",
            self.keys.ZIP: "94107",
            self.keys.DO_NOT_CALL: "TRUE",
        }
        unique.update(row)
        return unique

    def _existing_membership(self, row: Dict[str, str]) -> NationalMembershipData:
        return NationalMembershipData(
            ak_id=row[self.keys.AK_ID],
            first_name=row[self.keys.FIRST_NAME],
            last_name=row[self.keys.LAST_NAME],
            city=row[self.keys.CITY],
            country=row[self.keys.COUNTRY],
            zipcode=row[self.keys.ZIP],
        )

    # Tests

    # - Test phone numbers

    def test_find_existing_member_by_email(self):
        session = Session()
        existing = Member()
        out = StringIO()
        existing.email_address = "joe.schmoe@example.com"
        session.add(existing)
        session.commit()
        found = self.job.find_existing_members(
            {self.keys.EMAIL: existing.email_address}, session, out
        )
        assert found[0][0].email_address == existing.email_address

    def test_find_existing_member_by_normalized_email(self):
        session = Session()
        existing = Member()
        out = StringIO()
        existing.normalized_email = Member.normalize_email("joe.schmoe@example.com")
        session.add(existing)
        session.commit()
        found = self.job.find_existing_members(
            {self.keys.EMAIL: existing.normalized_email}, session, out
        )
        assert found[0][0].normalized_email == existing.normalized_email

    def test_find_existing_member_by_first_and_last_name(self):
        session = Session()
        existing = Member()
        out = StringIO()
        existing.first_name = "Joe"
        existing.last_name = "Schmoe"
        session.add(existing)
        session.commit()
        found = self.job.find_existing_members(
            {
                self.keys.FIRST_NAME: existing.first_name,
                self.keys.LAST_NAME: existing.last_name,
            },
            session,
            out,
        )
        assert found[0][0].first_name == existing.first_name
        assert found[0][0].last_name == existing.last_name

    def test_find_existing_member_by_first_and_last_name_ignore_case(self):
        session = Session()
        existing = Member()
        out = StringIO()
        existing.first_name = "Joe"
        existing.last_name = "Schmoe"
        session.add(existing)
        session.commit()
        found = self.job.find_existing_members(
            {
                self.keys.FIRST_NAME: existing.first_name.upper(),
                self.keys.LAST_NAME: existing.last_name.upper(),
            },
            session,
            out,
        )
        assert found[0][0].first_name == existing.first_name
        assert found[0][0].last_name == existing.last_name

    def test_find_existing_member_by_first_name_only_is_empty(self):
        session = Session()
        existing = Member()
        out = StringIO()
        existing.first_name = "Joe"
        session.add(existing)
        session.commit()
        found = self.job.find_existing_members(
            {self.keys.FIRST_NAME: existing.first_name}, session, out
        )
        assert found == []

    def test_find_existing_member_by_last_name_only_is_empty(self):
        session = Session()
        existing = Member()
        out = StringIO()
        existing.last_name = "Schmoe"
        session.add(existing)
        session.commit()
        found = self.job.find_existing_members(
            {self.keys.LAST_NAME: existing.last_name}, session, out
        )
        assert found == []

    def test_find_existing_member_ignores_deleted(self):
        session = Session()
        ignore = Member()
        out = StringIO()
        ignore.first_name = "Joe"
        ignore.last_name = "Schmoe"
        ignore.email_address = "DELETE"
        session.add(ignore)
        session.flush()
        valid = Member(
            first_name="Joe", last_name="Schmoe", email_address="joe.schmoe@example.com"
        )
        session.add(valid)
        session.commit()
        found = self.job.find_existing_members(
            {self.keys.FIRST_NAME: "Joe", self.keys.LAST_NAME: "Schmoe"}, session, out
        )
        assert found[0][0].email_address == valid.email_address

    def test_find_existing_member_by_additional_email(self):
        session = Session()
        existing = Member(id=1)
        out = StringIO()
        additional_email = AdditionalEmailAddress(
            member_id=existing.id, name="foo", email_address="hello@example.com"
        )
        session.add(existing)
        session.add(additional_email)
        session.commit()
        found = self.job.find_existing_members(
            {self.keys.EMAIL: "hello@example.com"}, session, out
        )
        found_address = found[0][0].additional_email_addresses[0]
        assert found[0][0].id == 1
        assert found_address is not None
        assert found_address.email_address == additional_email.email_address

    # - Test import members

    def test_import_member(self):
        rows = [self._unique_row({})]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.members_created == 1
        assert results.members_updated == 0

    def test_import_member_updates_existing(self):
        session = Session()
        existing_member = Member(
            first_name="Joe", last_name="Schmoe", email_address="joe.schmoe@example.com"
        )
        session.add(existing_member)
        session.commit()
        session.refresh(existing_member)
        row = self._unique_row(
            {
                self.keys.FIRST_NAME: existing_member.first_name,
                self.keys.LAST_NAME: existing_member.last_name,
                self.keys.DO_NOT_CALL: "TRUE",
            }
        )
        session.add(self._existing_membership(row))
        session.add(Role(member_id=existing_member.id, role="member"))
        session.commit()
        rows = [row]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.members_created == 0
        assert results.members_updated == 1

    # - Test import memberships

    def test_import_membership(self):
        rows = [self._unique_row({})]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.memberships_created == 1
        assert results.memberships_updated == 0

    def test_import_membership_updates_existing(self):
        session = Session()
        data = self._unique_row({})
        existing_membership = self._existing_membership(data)
        session.add(existing_membership)
        session.commit()
        data = self._unique_row(
            {
                self.keys.AK_ID: data[self.keys.AK_ID],
                self.keys.FIRST_NAME: "Updated",
                self.keys.LAST_NAME: "Updated",
                self.keys.CITY: "Updated",
                self.keys.COUNTRY: "Updated",
                self.keys.ZIP: "23456",
            }
        )
        rows = [data]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.memberships_created == 0
        assert results.memberships_updated == 1
        found: NationalMembershipData = session.query(NationalMembershipData).filter_by(
            ak_id=existing_membership.ak_id
        ).one()
        assert found.first_name == data[self.keys.FIRST_NAME]
        assert found.last_name == data[self.keys.LAST_NAME]
        assert found.city == data[self.keys.CITY]
        assert found.country == data[self.keys.COUNTRY]
        assert found.zipcode == data[self.keys.ZIP]

    # - Test extract phone numbers

    def test_extract_phone_numbers_usa(self):
        rows = [
            self._unique_row(
                {
                    self.keys.HOME_PHONE: "555-111-1111",
                    self.keys.MOBILE_PHONE: "555-222-2222",
                    self.keys.WORK_PHONE: "555-333-3333",
                }
            )
        ]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.phone_numbers_created == 3

    def test_extract_phone_numbers_skips_empty_strings(self):
        rows = [
            self._unique_row(
                {
                    self.keys.HOME_PHONE: "",
                    self.keys.MOBILE_PHONE: "",
                    self.keys.WORK_PHONE: "",
                }
            )
        ]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.phone_numbers_created == 0

    def test_extract_phone_numbers_skips_duplicates(self):
        rows = [
            self._unique_row(
                {
                    self.keys.HOME_PHONE: "555-111-1111",
                    self.keys.MOBILE_PHONE: "555-111-1111",
                    self.keys.WORK_PHONE: "555-111-1111",
                }
            )
        ]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.phone_numbers_created == 1

    def test_extract_phone_numbers_skips_errors(self):
        rows = [self._unique_row({self.keys.HOME_PHONE: "invalid"})]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.phone_numbers_created == 0

    # - Test extract identities

    def test_extract_identities(self):
        rows = [self._unique_row({self.keys.AK_ID: "1", self.keys.DSA_ID: "1"})]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.identities_created == 2

    def test_extract_identities_skips_empty_dsa_id(self):
        rows = [self._unique_row({self.keys.AK_ID: "1", self.keys.DSA_ID: ""})]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.identities_created == 1

    def test_extract_identities_skips_duplicates(self):
        rows = [
            self._unique_row({self.keys.AK_ID: "1", self.keys.DSA_ID: "1"}),
            self._unique_row({self.keys.AK_ID: "1", self.keys.DSA_ID: "1"}),
        ]
        results = self.job.import_all(rows, len(rows), **self.default_import_args)
        assert results.identities_created == 2
