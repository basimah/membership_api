import json
from datetime import datetime, timezone, timedelta
from unittest.mock import patch

from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import (
    MeetingInvitation,
    MeetingInvitationStatus,
    Attendee,
    Committee,
    Meeting,
    Member,
    Role,
    MeetingAgenda,
)
from membership.web.base_app import app
from tests.flask_utils import get_json, patch_json, post_json, delete_json


class TestWebMeetings:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        # set up for auth
        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        m.first_name = "Eugene"
        m.last_name = "Debs"
        session.add(m)
        role = Role()
        role.member = m
        role.role = 'admin'
        session.add(role)
        committee1 = Committee(id=1, name='Committee 1')
        session.commit()

        meeting1 = Meeting(
            id=1,
            short_id=1,
            name='Meeting 1',
            start_time=datetime(2017, 1, 1, 0),
            end_time=datetime(2017, 1, 1, 1),
            owner_id=m.id,
        )
        meeting2 = Meeting(
            id=2,
            short_id=2,
            name='Meeting 2',
            start_time=datetime(2017, 2, 1, 0),
            end_time=datetime(2017, 2, 1, 1),
            owner_id=m.id,
        )
        meeting3 = Meeting(
            id=3,
            short_id=3,
            name='Meeting 3',
            committee_id=committee1.id,
            start_time=datetime(2017, 3, 1, 0),
            end_time=datetime(2017, 3, 1, 1),
            owner_id=m.id,
        )
        meeting4 = Meeting(
            id=4,
            short_id=None,
            name='Meeting 4',
            committee_id=committee1.id,
            start_time=datetime(2017, 4, 1, 0),
            end_time=datetime(2017, 4, 1, 1),
            owner_id=m.id,
        )
        meeting5 = Meeting(
            id=5,
            short_id=None,
            name='General Meeting 5',
            start_time=datetime(2017, 5, 1, 0),
            end_time=datetime(2017, 5, 1, 1),
            owner_id=m.id,
        )
        meeting6 = Meeting(
            id=6,
            short_id=None,
            name='General Meeting 6',
            start_time=datetime(2017, 6, 1, 0),
            end_time=datetime(2017, 6, 1, 1),
            owner_id=m.id,
        )
        meeting7 = Meeting(
            id=7,
            short_id=7,
            name='General Meeting 7',
            start_time=datetime(2017, 7, 1, 0),
            end_time=datetime(2017, 7, 1, 1),
            owner_id=m.id,
        )

        attendee2 = Attendee(
            meeting=meeting2,
            member=m
        )
        attendee3 = Attendee(
            meeting=meeting3,
            member=m
        )
        session.add_all([
            committee1,
            meeting1,
            attendee2,
            attendee3,
            meeting4,
            meeting5,
            meeting6,
            meeting7
        ])

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_create_meeting(self):
        name = 'New Meeting'
        landing_url = 'docs.google.com/my_landing_url'
        committee_id = 1
        code = 5678
        start_time = '2017-05-01T00:00:00+00:00'
        end_time = '2017-05-01T01:00:00+00:00'
        payload = {
            'name': name,
            'code': code,
            'committee_id': committee_id,
            'landing_url': landing_url,
            'start_time': start_time,
            'end_time': end_time
        }

        response = post_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'meeting': {
                'code': code,
                'committee_id': committee_id,
                'id': 8,
                'landing_url': landing_url,
                'name': 'New Meeting',
                'start_time': start_time,
                'end_time': end_time,
                'owner': 'Eugene Debs',
                'published': True
            },
            'status': 'success'
        }

        session = Session()
        assert session.query(Meeting).count() == 8

    def test_create_meeting_duplicate(self):
        payload = {'name': 'Meeting 1'}
        response = post_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 400

        session = Session()
        assert session.query(Meeting).count() == 7

    def test_create_meeting_invalid(self):
        payload = {'name': 'New Meeting', 'code': 12345}
        response = post_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 400

        session = Session()
        assert session.query(Meeting).count() == 7

    def test_create_update_meeting_non_json(self):
        response = self.app.post('/meeting', data=None)
        assert response.status_code == 400

        response = self.app.patch('/meeting', data=None)
        assert response.status_code == 400

    def test_get_meetings(self):
        response = get_json(self.app, '/meeting/list')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [
            {
                'id': 7,
                'name': 'General Meeting 7',
                'committee_id': None,
                'code': 7,
                'landing_url': None,
                'start_time': '2017-07-01T00:00:00+00:00',
                'end_time': '2017-07-01T01:00:00+00:00',
                'owner': 'Eugene Debs',
                'published': True
            },
            {
                'id': 6,
                'name': 'General Meeting 6',
                'committee_id': None,
                'code': None,
                'landing_url': None,
                'start_time': '2017-06-01T00:00:00+00:00',
                'end_time': '2017-06-01T01:00:00+00:00',
                'owner': 'Eugene Debs',
                'published': True
            },
            {
                'id': 5,
                'name': 'General Meeting 5',
                'committee_id': None,
                'code': None,
                'landing_url': None,
                'start_time': '2017-05-01T00:00:00+00:00',
                'end_time': '2017-05-01T01:00:00+00:00',
                'owner': 'Eugene Debs',
                'published': True
            },
            {
                'id': 4,
                'name': 'Meeting 4',
                'committee_id': 1,
                'code': None,
                'landing_url': None,
                'start_time': '2017-04-01T00:00:00+00:00',
                'end_time': '2017-04-01T01:00:00+00:00',
                'owner': 'Eugene Debs',
                'published': True
            },
            {
                'id': 3,
                'name': 'Meeting 3',
                'committee_id': 1,
                'code': 3,
                'landing_url': None,
                'start_time': '2017-03-01T00:00:00+00:00',
                'end_time': '2017-03-01T01:00:00+00:00',
                'owner': 'Eugene Debs',
                'published': True
            },
            {
                'id': 2,
                'name': 'Meeting 2',
                'committee_id': None,
                'code': 2,
                'landing_url': None,
                'start_time': '2017-02-01T00:00:00+00:00',
                'end_time': '2017-02-01T01:00:00+00:00',
                'owner': 'Eugene Debs',
                'published': True
            },
            {
                'id': 1,
                'name': 'Meeting 1',
                'committee_id': None,
                'code': 1,
                'landing_url': None,
                'start_time': '2017-01-01T00:00:00+00:00',
                'end_time': '2017-01-01T01:00:00+00:00',
                'owner': 'Eugene Debs',
                'published': True
            },
        ]

    def test_update_meeting_landing_url(self):
        landing_url = 'docs.google.com/my_landing_url'
        payload = {
            'meeting_id': 2,
            'landing_url': landing_url,
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['landing_url'] == landing_url

    def test_update_meeting_landing_url_not_found(self):
        payload = {
            'meeting_id': 8398,
            'landing_url': 'docs.google.com/my_landing_url',
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 404

    def test_update_meeting_code(self):
        payload = {
            'meeting_id': 3,
            'code': '1234'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['code'] == 1234

    def test_unset_meeting_code(self):
        payload = {
            'meeting_id': 3,
            'code': None
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['code'] is None

    def test_update_meeting_code_autogenerate(self):
        payload = {
            'meeting_id': 4,
            'code': 'autogenerate'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert 1000 <= result['meeting']['code'] <= 9999

    def test_update_meeting_code_autogenerate_existing_code(self):
        payload = {
            'meeting_id': 3,
            'code': 'autogenerate'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert 1000 <= result['meeting']['code'] <= 9999

    def test_update_meeting_code_invalid_code(self):
        payload = {
            'meeting_id': 1,
            'code': '000F'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 400

    def test_update_meeting_null_code(self):
        payload = {
            'meeting_id': 1,
            'code': None
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

    def test_update_meeting_code_meeting_not_found(self):
        payload = {
            'meeting_id': 82,
            'code': '1234'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 404

    def test_update_meeting_times(self):
        payload = {
            'meeting_id': 4,
            'start_time': '2017-04-02T00:00:00+00:00',
            'end_time': '2017-04-02T01:00:00+00:00'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['start_time'] == '2017-04-02T00:00:00+00:00'
        assert result['meeting']['end_time'] == '2017-04-02T01:00:00+00:00'

    def test_update_meeting_name(self):
        payload = {
            'meeting_id': 4,
            'name': 'Talent Show'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['name'] == 'Talent Show'

    def test_update_meeting_clear_values(self):
        payload = {
            'meeting_id': 4,
            'landing_url': None,
            'start_time': None,
            'end_time': None
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['landing_url'] is None
        assert result['meeting']['start_time'] is None
        assert result['meeting']['end_time'] is None

    def test_attend_meeting(self):
        payload = {'meeting_short_id': 1}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success', 'landing_url': None}

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

    def test_attend_meeting_invalid_code(self):
        payload = {'meeting_short_id': 'a'}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 400

    def test_attend_meeting_duplicate(self):
        payload = {'meeting_short_id': 2}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 400

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=2, member_id=1).count() == 1

    def test_attend_meeting_as_admin_with_somebody_else(self):
        somebody_else = Member(id=2)
        session = Session()
        session.add(somebody_else)
        session.commit()

        payload = {'meeting_short_id': 1, 'member_id': somebody_else.id}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success', 'landing_url': None}

        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 0
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=2).count() == 1

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(2017, 7, 1, 1).replace(tzinfo=timezone.utc)
    )
    def test_attend_meeting_with_eligibility_at_attendance(self, get_current_time_mock):
        session = Session()
        member = session.query(Member).get(1)
        role = Role(member=member, role='member', committee_id=None)
        meeting5 = session.query(Meeting).get(5)
        meeting6 = session.query(Meeting).get(6)
        attendee5 = Attendee(member=member, meeting=meeting5)
        attendee6 = Attendee(member=member, meeting=meeting6)
        session.add_all([role, attendee5, attendee6])
        session.commit()

        payload = {'meeting_short_id': 7}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success', 'landing_url': None}

        attendee = session.query(Attendee).filter_by(meeting_id=7, member=member).one()
        assert attendee.eligible_to_vote is True

    def test_attend_meeting_meeting_not_found(self):
        payload = {'meeting_short_id': 1337}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 404

    def test_attend_meeting_non_json(self):
        response = self.app.post('/meeting/attend', data=None)
        assert response.status_code == 400

    def test_get_meeting(self):
        response = get_json(self.app, '/meetings/1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'name': 'Meeting 1',
            'committee_id': None,
            'code': 1,
            'landing_url': None,
            'start_time': '2017-01-01T00:00:00+00:00',
            'end_time': '2017-01-01T01:00:00+00:00',
            'owner': 'Eugene Debs',
            'published': True
        }

    def test_get_meeting_not_found(self):
        response = get_json(self.app, '/meetings/1337')
        assert response.status_code == 404

    def test_get_meeting_attendees(self):
        response = get_json(self.app, '/meetings/3/attendees')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [
            {'id': 1, 'name': 'Eugene Debs'},
        ]

    def test_get_meeting_attendees_with_eligibility(self):
        session = Session()
        somebody_else = Member(id=2)
        attendee1 = Attendee(member_id=1, meeting_id=7, eligible_to_vote=True)
        attendee2 = Attendee(member_id=2, meeting_id=7)
        session.add_all([somebody_else, attendee1, attendee2])
        session.commit()

        response = get_json(self.app, '/meetings/7/attendees')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [
            {
                'id': 1,
                'name': 'Eugene Debs',
                'email': SUPER_USER_EMAIL,
                'eligibility': {'is_eligible': True, 'message': 'eligible', 'num_votes': 1}
            },
            {
                'id': 2,
                'name': '',
                'email': None,
                'eligibility': {'is_eligible': False, 'message': 'not eligible', 'num_votes': 0}
            },
        ]

    def test_get_meeting_attendees_meeting_not_found(self):
        response = get_json(self.app, '/meetings/1337/attendees')
        assert response.status_code == 404

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(2017, 7, 1, 1).replace(tzinfo=timezone.utc)
    )
    def test_update_meeting_attendee_eligibility_at_attendance(self, get_current_time_mock):
        session = Session()
        member = session.query(Member).get(1)
        attendee = Attendee(member=member, meeting_id=7)
        session.add(attendee)

        assert attendee.eligible_to_vote is None

        role_member = Role(
            member=member,
            role='member',
            committee_id=None
        )
        role_active = Role(
            member=member,
            role='active',
            committee_id=1
        )
        session.add_all([role_member, role_active])
        session.commit()

        payload = {
            'update_eligibility_to_vote': True,
        }
        response = patch_json(self.app, '/meetings/7/attendees/1', payload=payload)
        assert response.status_code == 200

        attendee = session.query(Attendee).filter_by(meeting_id=7, member_id=1).one()

        assert attendee.eligible_to_vote is True

        session.delete(role_member)
        session.delete(role_active)
        session.commit()

        response = patch_json(self.app, '/meetings/7/attendees/1', payload=payload)
        assert response.status_code == 200

        assert attendee.eligible_to_vote is False

    def test_remove_meeting_attendee(self):
        response = delete_json(self.app, '/meetings/3/attendees/1')
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=3, member_id=1).count() == 0

    def test_remove_meeting_attendee_meeting_not_found(self):
        response = delete_json(self.app, '/meetings/1337/attendees/1')
        assert response.status_code == 404

    def test_add_attendee_from_kiosk(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'phone_number': '0000000000',
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

        # Adding this attendee should not have created a new member
        assert session.query(Member).count() == 1

    def test_add_attendee_from_kiosk_new_member(self):
        email_address = 'emma.goldman@riseup.net'
        payload = {
            'email_address': email_address,
            'phone_number': '0000000000',
            'first_name': 'Emma',
            'last_name': 'Goldman',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        member = session.query(Member).filter_by(email_address=email_address).one_or_none()
        assert member is not None
        assert member.first_name == 'Emma'
        assert member.last_name == 'Goldman'
        assert member.email_address == 'emma.goldman@riseup.net'
        assert member.phone_numbers[0].number == '+1 0000000000'
        assert member.normalized_email == 'emma.goldman@riseup.net'
        assert member.date_created is not None

        assert session.query(Attendee).filter_by(meeting_id=1, member_id=member.id).count() == 1

    def test_add_attendee_from_kiosk_duplicate_attendee(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'phone_number': '0000000000',
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

    def test_add_attendee_from_kiosk_update_first_name(self):
        email = 'emma@example.com'

        session = Session()
        member = Member(email_address=email)
        session.add(member)
        session.commit()

        payload = {
            'email_address': email,
            'phone_number': None,
            'first_name': 'Emma',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        member = session.query(Member).filter(Member.email_address == email).first()

        assert member.first_name == 'Emma'

    def test_add_attendee_from_kiosk_update_last_name(self):
        email = 'emma@example.com'

        session = Session()
        member = Member(email_address=email)
        session.add(member)
        session.commit()

        payload = {
            'email_address': email,
            'phone_number': None,
            'first_name': '',
            'last_name': 'Goldman',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        member = session.query(Member).filter(Member.email_address == email).first()

        assert member.last_name == 'Goldman'

    def test_add_attendee_from_kiosk_update_pronouns(self):
        email = 'emma@example.com'

        session = Session()
        member = Member(email_address=email)
        session.add(member)
        session.commit()

        payload = {
            'email_address': email,
            'phone_number': '',
            'first_name': '',
            'last_name': '',
            'pronouns': 'they/them'
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        member = session.query(Member).filter(Member.email_address == email).first()

        assert member.pronouns == 'they/them'

    def test_add_attendee_from_kiosk_emergency_contact(self):
        email = 'emma@example.com'

        session = Session()
        member = Member(email_address=email)
        session.add(member)
        session.commit()

        payload = {
            'email_address': email,
            'phone_number': '123-234-3456',
            'first_name': '',
            'last_name': '',
            'emergency_phone_number': '000-000-0000'
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        member = session.query(Member).filter(Member.email_address == email).first()
        assert member.phone_numbers[0].number == '+1 0000000000'
        assert member.phone_numbers[1].number == '+1 1232343456'

    def test_add_attendee_from_kiosk_guardian_contact(self):
        email = 'emma@example.com'

        session = Session()
        member = Member(email_address=email)
        session.add(member)
        session.commit()

        payload = {
            'email_address': email,
            'phone_number': '123-234-3456',
            'first_name': '',
            'last_name': '',
            'emergency_phone_number': '234-345-4567',
            'guardian_phone_number': '000-000-0000'
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        member = session.query(Member).filter(Member.email_address == email).first()
        assert member.phone_numbers[0].number == '+1 0000000000'
        assert member.phone_numbers[1].number == '+1 1232343456'
        assert member.phone_numbers[2].number == '+1 234-345-4567'

    def test_add_attendee_from_kiosk_blank_pronouns(self):
        email = 'emma@example.com'

        session = Session()
        member = Member(email_address=email)
        session.add(member)
        session.commit()

        payload = {
            'email_address': email,
            'phone_number': '',
            'first_name': '',
            'last_name': '',
            'pronouns': ''
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        member = session.query(Member).filter(Member.email_address == email).first()

        assert member.pronouns == ''

    def test_add_attendee_from_kiosk_empty_payload(self):
        payload = {}
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 400

    def test_add_attendee_from_kiosk_bad_email(self):
        payload = {
            'email_address': 'foo@bar.com',
            'phone_number': '',
            'first_name': '',
            'last_name': ''
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        # SECURITY: Avoid discovery attacks by verifying email
        assert response.status_code == 200

    def test_add_attendee_from_kiosk_no_email(self):
        payload = {
            'email_address': None,
            'phone_number': '0000000000',
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 400

    def test_add_attendee_from_kiosk_no_phone_number(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'phone_number': None,
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

    def test_add_attendee_from_kiosk_meeting_not_found(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'phone_number': '0000000000',
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1337/attendee', payload=payload)
        assert response.status_code == 404

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(2017, 7, 1, 1).replace(tzinfo=timezone.utc)
    )
    def test_add_attendee_from_kiosk_with_eligibility_at_attendance(self, get_current_time_mock):
        session = Session()
        member = session.query(Member).get(1)
        role = Role(member=member, role='member', committee_id=None)
        meeting5 = session.query(Meeting).get(5)
        meeting6 = session.query(Meeting).get(6)
        attendee5 = Attendee(member=member, meeting=meeting5)
        attendee6 = Attendee(member=member, meeting=meeting6)
        session.add_all([role, attendee5, attendee6])
        session.commit()

        payload = {
            'email_address': SUPER_USER_EMAIL,
            'phone_number': '0000000000',
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/7/attendee', payload=payload)
        assert response.status_code == 200

        attendee = session.query(Attendee).filter_by(meeting_id=7, member=member).one()
        assert attendee.eligible_to_vote is True

    def test_add_attendee_from_kiosk_non_json(self):
        response = self.app.post('/meetings/7/attendee', data=None)
        assert response.status_code == 400

    def test_get_invitations(self):
        session = Session()
        member1 = Member(first_name='Karl', last_name='Marx', email_address='karl@marx.org')
        member2 = Member(first_name='Fred', last_name='Engels', email_address='fred@engels.org')
        session.add_all([member1, member2])
        session.commit()
        invitation1 = MeetingInvitation(member_id=member1.id, meeting_id=1)
        invitation2 = MeetingInvitation(
            member_id=member2.id,
            meeting_id=1,
            status=MeetingInvitationStatus.ACCEPTED.value,
        )
        session.add_all([invitation1, invitation2])
        session.commit()

        # You can't get invitations for a meeting that doesn't exist
        response = self.app.get('/meetings/1917/invitations')
        assert response.status_code == 404

        response = self.app.get('/meetings/1/invitations')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == [{
            'id': invitation1.id,
            'meeting_id': 1,
            'status': 'SEND_PENDING',
            'created_at': invitation1.created_at.isoformat(),
            'name': 'Karl Marx',
            'email': 'karl@marx.org',
        }, {
            'id': invitation2.id,
            'meeting_id': 1,
            'status': 'ACCEPTED',
            'created_at': invitation2.created_at.isoformat(),
            'name': 'Fred Engels',
            'email': 'fred@engels.org',
        }]

    def test_send_invitation(self):
        session = Session()
        member = Member(first_name='Karl', last_name='Marx', email_address='karl@marx.org')
        session.add(member)
        session.commit()

        # You can't invite someone to a meeting that doesn't exist
        response = post_json(
            self.app,
            '/meetings/1917/invitation',
            payload={'member_id': member.id},
        )
        assert response.status_code == 404

        response = post_json(self.app, '/meetings/1/invitation', payload={'member_id': member.id})
        assert response.status_code == 201
        invitation = session.query(MeetingInvitation).one()
        result = json.loads(response.data)
        assert result == {
            'invitation': {
                'id': invitation.id,
                'meeting_id': 1,
                'status': 'NO_RESPONSE',
                'created_at': invitation.created_at.isoformat(),
                'name': 'Karl Marx',
                'email': 'karl@marx.org',
            },
            'status': 'success'
        }

        # You can't invite someone twice
        response = post_json(self.app, '/meetings/1/invitation', payload={'member_id': member.id})
        assert response.status_code == 400

    def test_bulk_send_invitations(self):
        session = Session()
        member = Member(first_name='Karl', last_name='Marx', email_address='karl@marx.org')
        role1 = Role(member=member, role='foo', committee_id=1)
        already_invited_member = Member(
            first_name='Fred', last_name='Engels',
            email_address='fred@engels.org',
        )
        role2 = Role(member=already_invited_member, role='bar', committee_id=1)
        prior_invitation = MeetingInvitation(member=already_invited_member, meeting_id=3)
        multiple_roles_member = Member(
            first_name='Vlad', last_name='Lenin',
            email_address='vlad@ussr.gov',
        )
        role3 = Role(member=multiple_roles_member, role='baz', committee_id=1)
        invitation_to_other_meeting = MeetingInvitation(member=multiple_roles_member, meeting_id=4)
        role4 = Role(member=multiple_roles_member, role='hey', committee_id=1)
        different_committee_member = Member(
            first_name='Leon', last_name='Trotsky',
            email_address='leon@trotsky.org',
        )
        committee2 = Committee(id=2, name='Committee 2')
        role5 = Role(member=different_committee_member, role='admin', committee_id=2)
        session.add_all([
            member,
            role1,
            already_invited_member,
            role2,
            prior_invitation,
            multiple_roles_member,
            role3,
            invitation_to_other_meeting,
            role4,
            different_committee_member,
            committee2,
            role5,
        ])
        session.commit()

        # No bulk-inviting to a meeting that doesn't exist
        response = self.app.post('/meetings/1917/invite-all-committee-members')
        assert response.status_code == 404

        # No bulk-inviting to meetings without committees
        response = self.app.post('/meetings/1/invite-all-committee-members')
        assert response.status_code == 404

        # Bulk invitations should invite members of the meeting's committee
        # that aren't already invited and should not send duplicate invites
        response = self.app.post('/meetings/3/invite-all-committee-members')
        assert response.status_code == 201
        invitations = (
            session.query(MeetingInvitation)
            .filter(MeetingInvitation.id.notin_([
                prior_invitation.id,
                invitation_to_other_meeting.id,
            ])).all()
        )
        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert len(result['invitations']) == 2
        marx_invite = [i for i in result['invitations'] if i['name'] == 'Karl Marx'][0]
        assert marx_invite == {
            'id': invitations[0].id,
            'meeting_id': 3,
            'status': 'NO_RESPONSE',
            'created_at': invitations[0].created_at.isoformat(),
            'name': 'Karl Marx',
            'email': 'karl@marx.org',
        }
        lenin_invite = [i for i in result['invitations'] if i['name'] == 'Vlad Lenin'][0]
        assert lenin_invite == {
            'id': invitations[1].id,
            'meeting_id': 3,
            'status': 'NO_RESPONSE',
            'created_at': invitations[1].created_at.isoformat(),
            'name': 'Vlad Lenin',
            'email': 'vlad@ussr.gov',
        }

    def test_rsvp(self):
        session = Session()
        invitation = MeetingInvitation(member_id=1, meeting_id=3)
        member = Member(first_name='Karl', last_name='Marx', email_address='karl@marx.org')
        others_invitation = MeetingInvitation(member=member, meeting_id=3)
        session.add_all([invitation, member, others_invitation])
        session.commit()

        # You can't respond to an invite for a meeting that doesn't exist
        response = self.app.post('/meetings/1917/invitation/ACCEPTED')
        assert response.status_code == 404

        # You can't respond to an invite that doesn't exist
        response = self.app.post('/meetings/2/invitation/ACCEPTED')
        assert response.status_code == 404

        response = self.app.post(f'/meetings/3/invitation/ACCEPTED')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == {
            'invitation': {
                'id': invitation.id,
                'meeting_id': 3,
                'status': 'ACCEPTED',
                'created_at': invitation.created_at.isoformat(),
                'name': 'Eugene Debs',
                'email': SUPER_USER_EMAIL,
            },
            'status': 'success',
        }

        # You can change your response
        response = self.app.post(f'/meetings/3/invitation/DECLINED')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == {
            'invitation': {
                'id': invitation.id,
                'meeting_id': 3,
                'status': 'DECLINED',
                'created_at': invitation.created_at.isoformat(),
                'name': 'Eugene Debs',
                'email': SUPER_USER_EMAIL,
            },
            'status': 'success',
        }

    def test_get_my_invitations(self):
        session = Session()
        me = session.query(Member).filter_by(email_address=SUPER_USER_EMAIL).one()
        other_member = Member(first_name='Karl', last_name='Marx', email_address='karl@marx.org')
        session.add_all([other_member])
        session.commit()
        invitation1 = MeetingInvitation(member_id=me.id, meeting_id=1)
        invitation2 = MeetingInvitation(
            member_id=me.id,
            meeting_id=2,
            status=MeetingInvitationStatus.DECLINED.value,
        )
        invitation3 = MeetingInvitation(
            member_id=other_member.id,
            meeting_id=1,
            status=MeetingInvitationStatus.ACCEPTED.value,
        )
        session.add_all([invitation1, invitation2, invitation3])
        session.commit()

        response = self.app.get('/invitations')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == [{
            'id': invitation1.id,
            'meeting_id': 1,
            'status': 'SEND_PENDING',
            'created_at': invitation1.created_at.isoformat(),
            'name': 'Eugene Debs',
            'email': SUPER_USER_EMAIL,
        }, {
            'id': invitation2.id,
            'meeting_id': 2,
            'status': 'DECLINED',
            'created_at': invitation2.created_at.isoformat(),
            'name': 'Eugene Debs',
            'email': SUPER_USER_EMAIL,
        }]

    def test_publish_vs_invite(self):
        session = Session()
        owner = session.query(Member).filter(Member.email_address == SUPER_USER_EMAIL).one()
        unpublished_meeting = Meeting(
            id=8,
            short_id=8,
            name='Meeting 8',
            committee_id=1,
            start_time=datetime(2017, 4, 1, 0),
            end_time=datetime(2017, 4, 1, 1),
            owner_id=owner.id,
            published=False,
        )
        session.add(unpublished_meeting)
        session.commit()
        member = Member(first_name='Karl', last_name='Marx', email_address='karl@marx.org')
        role1 = Role(member=member, role='foo', committee_id=1)
        already_invited_member = Member(
            first_name='Fred', last_name='Engels',
            email_address='fred@engels.org',
        )
        role2 = Role(member=already_invited_member, role='bar', committee_id=1)
        prior_invitation = MeetingInvitation(
            member=already_invited_member,
            meeting_id=unpublished_meeting.id,
        )
        multiple_roles_member = Member(
            first_name='Vlad', last_name='Lenin',
            email_address='vlad@ussr.gov',
        )
        role3 = Role(member=multiple_roles_member, role='baz', committee_id=1)
        role4 = Role(member=multiple_roles_member, role='hey', committee_id=1)
        different_committee_member = Member(
            first_name='Leon', last_name='Trotsky',
            email_address='leon@trotsky.org',
        )
        committee2 = Committee(id=2, name='Committee 2')
        role5 = Role(member=different_committee_member, role='admin', committee_id=2)
        session.add_all([
            member,
            role1,
            already_invited_member,
            role2,
            prior_invitation,
            multiple_roles_member,
            role3,
            role4,
            different_committee_member,
            committee2,
            role5,
        ])
        session.commit()

        # If the meeting is unpublished, the invitation isn't sent by default
        response = post_json(self.app, '/meetings/8/invitation', payload={'member_id': member.id})
        assert response.status_code == 201
        marx_invite = (
            session.query(MeetingInvitation)
            .join(Member, MeetingInvitation.member_id == Member.id)
            .filter(Member.email_address == 'karl@marx.org').one()
        )
        result = json.loads(response.data)
        assert result == {
            'invitation': {
                'id': marx_invite.id,
                'meeting_id': 8,
                'status': 'SEND_PENDING',
                'created_at': marx_invite.created_at.isoformat(),
                'name': 'Karl Marx',
                'email': 'karl@marx.org',
            },
            'status': 'success'
        }

        # This is also true when you bulk invite
        response = self.app.post('/meetings/8/invite-all-committee-members')
        assert response.status_code == 201
        result = json.loads(response.data)
        assert result['status'] == 'success'
        # Only Lenin hasn't been invited yet
        assert len(result['invitations']) == 1
        lenin_invite = (
            session.query(MeetingInvitation)
            .join(Member, MeetingInvitation.member_id == Member.id)
            .filter(Member.email_address == 'vlad@ussr.gov').one()
        )
        lenin_invite_json = result['invitations'][0]
        assert lenin_invite_json == {
            'id': lenin_invite.id,
            'meeting_id': 8,
            'status': 'SEND_PENDING',
            'created_at': lenin_invite.created_at.isoformat(),
            'name': 'Vlad Lenin',
            'email': 'vlad@ussr.gov',
        }

        # Publish meeting, see that all invites are sent
        response = patch_json(self.app, '/meeting', payload={
            'meeting_id': 8,
            'published': True,
        })
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['published']
        session.close()
        session = Session()
        invites = (
            session.query(MeetingInvitation)
            .filter(MeetingInvitation.meeting_id == 8).all()
        )
        for invite in invites:
            assert invite.status == MeetingInvitationStatus.NO_RESPONSE

    def test_get_agenda(self):
        session = Session()
        agendas = session.query(MeetingAgenda).all()
        assert len(agendas) == 0

        # You can't get agendas for meetings that don't exist
        response = get_json(self.app, '/meetings/1917/agenda')

        response = get_json(self.app, '/meetings/1/agenda')
        assert response.status_code == 200
        agendas = session.query(MeetingAgenda).all()
        assert len(agendas) == 1
        agenda = agendas[0]
        response_json = json.loads(response.data)
        assert response_json == {
            'id': 1,
            'meeting_id': 1,
            'text': '',
            'updated_at': agenda.updated_at.isoformat()
        }

    def test_edit_agenda(self):
        session = Session()
        agendas = session.query(MeetingAgenda).all()
        assert len(agendas) == 0

        # You can't edit an agenda for a meeting that doesn't exist
        response = post_json(self.app, '/meetings/1917/agenda', {
            'text': 'foo',
            'checkout_timestamp': datetime.now().isoformat(),
        })
        assert response.status_code == 404

        # You can edit an agenda before one exists
        response = post_json(self.app, '/meetings/1/agenda', {
            'text': 'foo',
            'checkout_timestamp': datetime.now().isoformat(),
        })
        assert response.status_code == 200
        agendas = session.query(MeetingAgenda).all()
        assert len(agendas) == 1
        agenda = agendas[0]
        response_json = json.loads(response.data)
        assert response_json == {
            'status': 'success',
            'agenda': {
                'id': 1,
                'meeting_id': 1,
                'text': 'foo',
                'updated_at': agenda.updated_at.isoformat()
            }
        }

        # You can update an existing agenda
        response = post_json(self.app, '/meetings/1/agenda', {
            'text': 'bar',
            'checkout_timestamp': agenda.updated_at.isoformat(),
        })
        assert response.status_code == 200
        agendas = session.query(MeetingAgenda).all()
        assert len(agendas) == 1
        agenda = agendas[0]
        response_json = json.loads(response.data)
        assert response_json == {
            'status': 'success',
            'agenda': {
                'id': 1,
                'meeting_id': 1,
                'text': 'bar',
                'updated_at': agenda.updated_at.isoformat()
            }
        }

        # If you send up an old timestamp, you cannot edit the agenda
        response = post_json(self.app, '/meetings/1/agenda', {
            'text': 'bar',
            'checkout_timestamp': (agenda.updated_at - timedelta(minutes=1)).isoformat(),
        })
        assert response.status_code == 400
