import pytest

from datetime import datetime
from typing import List, Optional

from membership.database.base import engine, metadata, Session
from membership.database.models import Attendee, Member, Meeting
from membership.repos import MeetingRepo
from membership.services import AttendeeService
from membership.services.errors import ValidationError
from membership.util.time import get_current_time
from overrides import overrides


# TODO: use abstract base class with actual repository for interface
class FakeMeetingRepository(MeetingRepo):

    def __init__(self, meetings: List[Meeting]) -> None:
        self._all = meetings
        self._most_recent = None

    @overrides
    def most_recent(
        self,
        session: Session,
        limit: int = 3,
        since: Optional[datetime] = None
    ) -> List[Meeting]:
        if self._most_recent is None:
            if since is None:
                since = get_current_time()

            def meeting_ended_since(meeting: Meeting) -> bool:
                end_time = meeting.end_time

                if end_time == None:
                    return False

                return end_time < since

            def sort_by_start_time(meeting: Meeting) -> datetime:
                return meeting.start_time

            self._most_recent = sorted(
                filter(meeting_ended_since, self._all),
                key=sort_by_start_time
            )
        return self._most_recent[-limit:]


class TestAttendeeService:
    def setup(self):
        metadata.create_all(engine)

        session = Session()

        meeting1 = Meeting(
            id=1,
            name='General Meeting 1',
            short_id=1,
            start_time=datetime(2017, 1, 1),
            end_time=datetime(2017, 1, 1),
        )
        meeting2 = Meeting(
            id=2,
            name='General Meeting 2',
            short_id=2,
            start_time=datetime(2017, 2, 1),
            end_time=datetime(2017, 2, 1),
        )
        meeting3 = Meeting(
            id=3,
            name='General Meeting 3',
            short_id=3,
            start_time=datetime(2017, 3, 1),
            end_time=datetime(2017, 3, 1),
        )

        session.add_all([meeting1, meeting2, meeting3])
        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_attend_meeting_with_short_id(self):
        session = Session()

        expected_member = Member(
            id=1,
            meetings_attended=[],
        )

        service = AttendeeService()

        meeting = service.attend_meeting_with_short_id(expected_member, 3, session)
        attendee = session.query(Attendee).filter_by(member=expected_member, meeting=meeting).one()

        assert attendee in expected_member.meetings_attended

    def test_attend_meeting_with_short_id_when_already_attended(self):
        session = Session()
        meeting3 = session.query(Meeting).get(3)
        attendee3 = Attendee(member_id=1, meeting=meeting3)

        expected_member = Member(
            id=1,
            meetings_attended=[attendee3],
        )

        service = AttendeeService()

        with pytest.raises(
            ValidationError,
            match=r'Member has already attended meeting \(meeting code: 3\)'
        ):
            service.attend_meeting_with_short_id(expected_member, 3, session)

    def test_attend_meeting(self):
        session = Session()

        expected_member = Member(
            id=1,
            meetings_attended=[],
        )

        service = AttendeeService()

        attendee = service.attend_meeting(expected_member, 3, session)

        assert attendee in expected_member.meetings_attended

    def test_attend_meeting_when_already_attended(self):
        session = Session()
        meeting3 = session.query(Meeting).get(3)
        attendee3 = Attendee(member_id=1, meeting=meeting3)

        expected_member = Member(
            id=1,
            meetings_attended=[attendee3],
        )

        service = AttendeeService()

        with pytest.raises(
            ValidationError,
            match=r'Member has already attended meeting \(id: 3\)'
        ):
            service.attend_meeting(expected_member, meeting3, session)

    def test_attend_meeting_when_already_attended_without_error(self):
        session = Session()
        meeting3 = session.query(Meeting).get(3)
        attendee3 = Attendee(member_id=1, meeting=meeting3)

        expected_member = Member(
            id=1,
            meetings_attended=[attendee3],
        )

        service = AttendeeService()

        assert service.attend_meeting(
            expected_member,
            meeting3,
            session,
            return_none_on_error=True
        ) is None

    def test_retrieve_attendee_with_attendee(self):
        session = Session()
        member = Member(id=1)
        meeting = session.query(Meeting).get(1)
        attendee = Attendee(member=member, meeting=meeting)

        session.add(attendee)

        service = AttendeeService()

        assert service.retrieve_attendee(session, attendee=attendee) == attendee

    def test_retrieve_attendee_with_member_and_meeting(self):
        session = Session()
        member = Member(id=1)
        meeting = session.query(Meeting).get(1)
        attendee = Attendee(member=member, meeting=meeting)

        session.add(attendee)

        service = AttendeeService()

        assert service.retrieve_attendee(session, member=member, meeting=meeting) == attendee

    def test_retrieve_attendee_with_extra_arguments(self):
        session = Session()
        member = Member(id=1)
        meeting = session.query(Meeting).get(1)
        attendee = Attendee(member=member, meeting=meeting)

        session.add(attendee)

        service = AttendeeService()

        with pytest.raises(
            ValueError,
            match=r'Must provide a Member and Meeting, or an Attendee'
        ):
            service.retrieve_attendee(session, member=member, meeting=meeting, attendee=attendee)

    # FIXME: this functionali was moved to the serivce but these tests need to be updated to
    # reflect the API and functionality
    '''
    def test_eligible_when_first_two_of_three_meetings(self):
        attended_meetings = [
            Attendee(meeting_id=1),
            Attendee(meeting_id=2)
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = True

        assert result == expected

    def test_eligible_when_last_two_of_three_meetings(self):
        attended_meetings = [
            Attendee(meeting_id=2),
            Attendee(meeting_id=3),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = True

        assert result == expected

    def test_eligible_when_first_and_last_of_three_meetings(self):
        attended_meetings = [
            Attendee(meeting_id=1),
            Attendee(meeting_id=3),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = True

        assert result == expected

    def test_not_eligible_when_attended_one_meeting(self):
        attended_meetings = [
            Attendee(meeting_id=3),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = False

        assert result == expected

    def test_not_eligible_when_attended_no_meetings(self):
        attended_meetings = []
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = False

        assert result == expected

    def test_not_eligible_when_attended_multiple_meetings_too_far_in_the_past(self):
        attended_meetings = [
            Attendee(meeting_id=4),
            Attendee(meeting_id=1),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = False

        assert result == expected
    '''
