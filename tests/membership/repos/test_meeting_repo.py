from datetime import datetime

from membership.database.base import engine, metadata, Session
from membership.database.models import Attendee, Meeting, Member
from membership.repos import MeetingRepo


class TestMeetingRepo:
    @classmethod
    def setup_class(cls):
        metadata.create_all(engine)

    @classmethod
    def teardown_class(cls):
        metadata.drop_all(engine)

    def test_all(self):
        session = Session()

        member1 = Member(email_address='test+1@test.com', first_name='m1-name')
        member2 = Member(email_address='test+2@test.com', first_name='m2-name')
        meeting1 = Meeting(
            short_id=1,
            name='General Meeting 1',
            start_time=datetime(2017, 1, 1),
            end_time=datetime(2017, 1, 1),
        )
        meeting2 = Meeting(
            short_id=2,
            name='General Meeting 2',
            start_time=datetime(2017, 2, 1),
            end_time=datetime(2017, 2, 1),
        )
        meeting3 = Meeting(
            short_id=3,
            name='General Meeting 3',
            start_time=datetime(2017, 3, 1),
            end_time=datetime(2017, 3, 1),
            landing_url='www.meeting_agenda.com/meeting3'
        )
        attendee1 = Attendee(member=member1, meeting=meeting1)
        attendee2 = Attendee(member=member2, meeting=meeting1)
        attendee3 = Attendee(member=member1, meeting=meeting2)

        session.add_all([
            member1,
            member2,
            meeting1,
            meeting2,
            meeting3,
            attendee1,
            attendee2,
            attendee3,
        ])
        session.commit()

        result = MeetingRepo(Meeting).most_recent(session)
        expected = [
            meeting1,
            meeting2,
            meeting3,
        ]

        session.close()

        assert result == expected
