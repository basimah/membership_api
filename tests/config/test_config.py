from config.lib import Config


class TestConfig:
    prims = {
        'bool': True,
        'int': 1,
        'float': 1.2,
        'str': 'hello',
    }
    prims_config = Config({
        k: str(v) for k, v in prims.items()
    })

    def test_config_reads_primitives_as_str(self):
        for k in self.prims:
            assert self.prims_config.get_str(k) == str(self.prims[k])

    def test_config_reads_bool(self):
        assert self.prims_config.get_bool('bool') == self.prims['bool']

    def test_config_reads_int(self):
        assert self.prims_config.get_int('int') == self.prims['int']

    def test_config_reads_float(self):
        assert self.prims_config.get_float('float') == self.prims['float']

    def test_config_reads_str(self):
        assert self.prims_config.get_str('str') == self.prims['str']

    def test_config_reads_default(self):
        assert self.prims_config.get_str('missing', 'backup') == 'backup'

    def test_config_reads_convert(self):
        assert self.prims_config.get_str('int', convert=lambda v: int(v) + 1) == 2

    def test_config_reads_does_not_convert_or_else(self):
        assert self.prims_config.get_str('missing', 0, convert=lambda v: int(v) + 1) == 0

    def test_get_as_custom(self):
        custom = self.prims_config.get_as(Custom, 'str')
        assert custom.value == self.prims['str']

    def test_get_as_custom_or_else(self):
        custom = self.prims_config.get_as(Custom, 'wrong', Custom(''))
        assert custom.value == ''

    def test_get_as_custom_convert(self):
        custom = self.prims_config.get_as(Custom, 'str', convert=lambda v: Custom(f'{v} world'))
        assert custom.value == f'{self.prims["str"]} world'


class Custom:
    def __init__(self, value: str):
        self.value = value
