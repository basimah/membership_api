from collections import defaultdict

from typing import Callable, Iterable, List, Optional, DefaultDict  # NOQA: F401

from membership.database.base import Session
from membership.database.models import (
    Meeting,
    Member,
    Role,
    ProxyToken,
    ProxyTokenState,
    NationalMembershipData,
    PhoneNumber,
    AdditionalEmailAddress
)
from membership.models import Authorization, MemberAsEligibleToVote, MemberQueryResult, \
     MergeMemberFlags, MergeMemberFlaggedEligibleVoter, MergeMemberFlaggedProxyToken
from membership.repos import MeetingRepo
from membership.schemas import JsonObj
from membership.schemas.rest.generic import format_iterable
from membership.services import AttendeeService
from membership.services.eligibility import SanFranciscoEligibilityService


def format_eligible_member_list(
    az: Authorization,
    meeting: Optional[Meeting]=None,
    query_session=None
) -> Callable[[Iterable[MemberAsEligibleToVote]], List[JsonObj]]:
    tokens_by_receiver: DefaultDict[int, list] = defaultdict(list)
    tokens_by_sender: DefaultDict[int, list] = defaultdict(list)
    if meeting and query_session:
        attendee_member_ids = [attendee.member_id for attendee in meeting.attendees]
        accepted_proxy_tokens = (
            query_session.query(ProxyToken)
            .filter(ProxyToken.meeting_id == meeting.id)
            .filter(ProxyToken.state == ProxyTokenState.ACCEPTED)
            .filter(ProxyToken.receiving_member_id.in_(attendee_member_ids))
            .all()
        )
        for token in accepted_proxy_tokens:
            tokens_by_receiver[token.receiving_member_id].append(token)
        for token in accepted_proxy_tokens:
            tokens_by_sender[token.member_id].append(token)

    def format_eligible_member(eligible_member: MemberAsEligibleToVote) -> JsonObj:
        requester_is_admin = az.has_role('admin')
        email_address_viewable = az.has_role('admin', member_id=eligible_member.id)
        num_votes = 1 if eligible_member.is_eligible else 0
        num_votes += len(tokens_by_receiver[eligible_member.id])
        num_votes -= len(tokens_by_sender[eligible_member.id])

        return {
            'id': eligible_member.id,
            'name': eligible_member.name,
            'eligibility': {
                'is_eligible': eligible_member.is_eligible,
                'message': eligible_member.message,
                'num_votes': num_votes,
            },
            **({'email': eligible_member.email_address}
                if email_address_viewable or requester_is_admin else {}),
        }

    return format_iterable(format_eligible_member)


def format_query_member_list(az: Authorization, query_session=None):
    def format_queried_member(eligible_member: MemberAsEligibleToVote) -> JsonObj:
        requester_is_admin = az.has_role('admin')
        email_address_viewable = az.has_role('admin', member_id=eligible_member.id)
        num_votes = 1 if eligible_member.is_eligible else 0
        membership: Optional[NationalMembershipData] = max(
            eligible_member.memberships_usa, key=lambda m: m.dues_paid_until,
            default=None) if requester_is_admin else None

        return {
            'id': eligible_member.id,
            'name': eligible_member.name,
            'eligibility': {
                'is_eligible': eligible_member.is_eligible,
                'message': eligible_member.message,
                'num_votes': num_votes,
            },
            ** ({'email': eligible_member.email_address}
                if email_address_viewable or requester_is_admin else {}),
            ** ({'membership': format_membership(
                az, membership, phone_numbers=eligible_member.phone_numbers)}
                if membership is not None else {})
        }

    return format_iterable(format_queried_member)


def format_membership(az: Authorization,
                      membership: NationalMembershipData,
                      *,
                      internal: bool=False,
                      phone_numbers: Optional[List[PhoneNumber]]=None) -> JsonObj:
    if not az.has_role('admin'):
        return {}
    else:
        return {
            **({'_date_imported': membership.date_imported} if internal else {}),
            'active': membership.active,
            'ak_id': membership.ak_id,
            'do_not_call': membership.do_not_call,
            'first_name': membership.first_name,
            'middle_name': membership.middle_name,
            'last_name': membership.last_name,
            # 'address_line_1': membership.address_line_1,
            # 'address_line_2': membership.address_line_2,
            'city': membership.city,
            'zipcode': membership.zipcode,
            # 'address': membership.address,
            'join_date': membership.join_date,
            'dues_paid_until': membership.dues_paid_until,
            **({'phone_numbers': [phone_number.number for phone_number in phone_numbers]}
                if phone_numbers is not None else {}),
        }


def format_role(role: Role) -> JsonObj:
    return {
        'role': role.role,
        'committee_id': role.committee.id if role.committee else -1,
        'committee_name': role.committee.name if role.committee else 'general',
        # TODO: To be deprecated once the front end no longer depends on 'committee'
        'committee': role.committee.name if role.committee else 'general',
        'date_created': role.date_created
    }


def format_member_query_result(member_query_result: MemberQueryResult, az: Authorization) \
        -> JsonObj:
    return {
        'members': format_query_member_list(az)(member_query_result.members),
        'cursor': member_query_result.cursor,
        'has_more': member_query_result.has_more,
    }


def format_member_info(member: Member, az: Authorization) -> JsonObj:
    return {
        'first_name': member.first_name,
        'last_name': member.last_name,
        **(
            {'pronouns': member.pronouns}
            if member.pronouns is not None else {}
        ),
        **(
            {'biography': member.biography}
            if member.biography is not None else {}
        ),
        **(
            {'phone_numbers': format_phone_numbers_full(member.phone_numbers)}
            if az.has_role('admin', member_id=member.id)
            else {'phone_numbers': []}
        ),
        **(
            {'email_address': member.email_address}
            if az.has_role('admin', member_id=member.id)
            else {}
        ),
        **(
            {'additional_email_addresses': format_email_addresses(
                member.additional_email_addresses
            )}
            if (az.has_role('admin', member_id=member.id) and
                len(member.additional_email_addresses) > 0)
            else {}
        )
    }


def format_member_basics(member: Member, az: Authorization) -> JsonObj:
    return {
        'id': member.id,
        'info': format_member_info(member, az),
        'roles': [format_role(role) for role in member.roles],
    }


def get_latest_membership(member: Member) -> Optional[NationalMembershipData]:
    return max(member.memberships_usa, key=lambda m: m.dues_paid_until, default=None)


def format_member_full(session: Session, member: Member, az: Authorization) -> JsonObj:
    if az.member_id == member.id:
        membership = max(member.memberships_usa, key=lambda m: m.dues_paid_until, default=None)
    else:
        membership = None

    eligibility_service = SanFranciscoEligibilityService(
        meetings=MeetingRepo(Meeting),
        attendee_service=AttendeeService()
    )
    is_eligible = eligibility_service.is_member_eligible(session, member)

    return {
        **format_member_basics(member, az),
        **(
            {'notes': member.notes}
            if az.has_role('admin')
            else {}
        ),
        'do_not_call': bool(member.do_not_call),
        'do_not_email': bool(member.do_not_email),
        'is_eligible': is_eligible,
        'meetings': [
            {
                'meeting_id': attendee.meeting.id,
                'name': attendee.meeting.name,
            } for attendee in member.meetings_attended
        ],
        'votes': [
            {
                'election_id': eligible_vote.election_id,
                'election_name': eligible_vote.election.name,
                'election_status': eligible_vote.election.status,
                'voted': eligible_vote.voted,
            } for eligible_vote in member.eligible_votes
        ],
        **(
            {'membership':
                {
                    'address': membership.address,
                    'phone_numbers': format_phone_numbers(member.phone_numbers),
                    'dues_paid_until': membership.dues_paid_until
                }
             } if membership is not None else {}
        )
    }


def format_phone_numbers(phone_numbers: List[PhoneNumber]) -> List[str]:
    return [phone_number.number for phone_number in phone_numbers]


def format_phone_numbers_full(phone_numbers: List[PhoneNumber]):
    return [{
        'phone_number': phone_number.number,
        'name': phone_number.name
    } for phone_number in phone_numbers]


def format_email_addresses(email_addresses: List[AdditionalEmailAddress]):
    return [{
        'email_address': address.email_address,
        'name': address.name,
        'preferred': bool(address.preferred),
        'verified': bool(address.verified)
    } for address in email_addresses]


def format_merge_member_flags(flags: MergeMemberFlags) -> JsonObj:
    return {
        'flagged_eligible_voters':
            [format_flagged_eligible_voters(f) for f in flags.flagged_eligible_voters],
        'flagged_proxy_tokens':
            [format_flagged_proxy_tokens(f) for f in flags.flagged_proxy_tokens]
    }


def format_flagged_eligible_voters(flagged_voter: MergeMemberFlaggedEligibleVoter) -> JsonObj:
    return {
        'election_id': flagged_voter.election_id,
        'did_keep_id_vote': flagged_voter.did_keep_id_vote,
        'did_remove_id_vote': flagged_voter.did_remove_id_vote
    }


def format_flagged_proxy_tokens(flagged_token: MergeMemberFlaggedProxyToken) -> JsonObj:
    return {
        'member_id': flagged_token.member_id,
        'meeting_id': flagged_token.meeting_id,
        'receiving_member_id': flagged_token.receiving_member_id,
        'state': flagged_token.state.friendly_name
    }


add_member_role_request_schema = {
    'type': 'object',
    'properties': {
        'member_id': {'type': 'number'},
        'committee_id': {'type': ['number', 'null']},
        'role': {'type': 'string'}
    },
    'required': ['member_id', 'committee_id', 'role']
}


update_interests_request_schema = {
    'type': 'object',
    'properties': {
        'topics': {
            'type': 'array',
            'items': {
                'type': 'string'
            }
        }
    },
    'required': ['topics']
}


update_notes_request_schema = {
    'type': 'object',
    'properties': {
        'notes': {
            'type': 'string'
        }
    }
}


update_member_details_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'additionalProperties': False,
    'properties': {
        'first_name': {'type': 'string'},
        'last_name': {'type': 'string'},
        'pronouns': {'type': 'string'},
        'biography': {'type': 'string'},
        'do_not_call': {'type': 'boolean'},
        'do_not_email': {'type': 'boolean'},
    },
}


add_phone_number_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['phone_number'],
    'properties': {
        'name': {'type': 'string'},
        'phone_number': {'type': 'string'}
    }
}


delete_phone_number_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['phone_number'],
    'properties': {
        'phone_number': {'type': 'string'}
    }
}


add_email_address_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['email_address'],
    'properties': {
        'name': {'type': 'string'},
        'email_address': {
            'type': 'string',
            'pattern': r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
        },
        'preferred': {'type': 'boolean'}
    }
}


delete_email_address_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['email_address'],
    'properties': {
        'email_address': {
            'type': 'string',
            'pattern': r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
        },
    }
}

phone_number_schema = {
    'type': 'object',
    'required': ['phone_number', 'name'],
    'properties': {
        'phone_number': {'type': 'string'},
        'name': {'type': 'string'}
    }
}

email_address_schema = {
    'type': 'object',
    'required': ['email_address', 'name', 'preferred', 'verified'],
    'properties': {
        'email_address': {'type': 'string'},
        'name': {'type': 'string'},
        'preferred': {'type': 'boolean'},
        'verified': {'type': 'boolean'}
    }
}

member_info_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['first_name', 'last_name', 'phone_numbers'],
    'properties': {
        'first_name': {'type': 'string'},
        'last_name': {'type': ['string', 'null']},
        'pronouns': {'type': 'string'},
        'biography': {'type': 'string'},
        'phone_numbers': {
            'type': 'array',
            'items': phone_number_schema
        },
        'email_address': {'type': 'string'},
        'additional_email_addresses': {
            'type': 'array',
            'items': email_address_schema
        }
    }
}
