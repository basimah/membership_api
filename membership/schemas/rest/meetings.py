from membership.database.models import (MeetingAgenda, MeetingInvitation)
from membership.schemas.rest.generic import error_schema
from membership.schemas.rest.members import member_info_schema

update_meeting_attendee_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['email_address'],
    'properties': {
        'email_address': {
            'type': 'string',
            'pattern': r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)',
        },
        'first_name': {'type': 'string'},
        'last_name': {'type': 'string'},
        'phone_number': {'type': ['string', 'null']},
        'pronouns': {
            'type': 'string',
            'pattern': r'^(?:\w+/\w+(?:/\w+){0,3}(?:;?\w+/\w+(?:/\w+){0,3})*)?$'
        },
        'emergency_phone_number': {'type': 'string'},
        'guardian_phone_number': {'type': 'string'},
        'provisional': {'type': 'boolean'}
    },
}

verify_email_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['token'],
    'properties': {
        'token': {'type': 'string'}
    }
}

verify_email_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['status', 'member'],
    'properties': {
        'status': {'type': 'string'},
        'member': member_info_schema
    }
}

verify_email_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [verify_email_success_schema, error_schema],
}

invitation_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['id', 'status', 'created_at', 'name', 'email'],
    'properties': {
        'id': {'type': 'number'},
        'meeting_id': {'type': 'number'},
        'status': {'type': 'string'},
        'created_at': {'type': 'string'},
        'name': {'type': 'string'},
        'email': {'type': 'string'},
    }
}

get_meeting_invitations_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'array',
    'items': invitation_schema,
}
get_meeting_invitations_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [get_meeting_invitations_success_schema, error_schema],
}

send_meeting_invitation_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['member_id'],
    'properties': {
        'member_id': {'type': 'number'},
    }
}

send_meeting_invitation_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['status', 'invitation'],
    'properties': {
        'status': {'type': 'string'},
        'invitation': invitation_schema,
    }
}
send_meeting_invitation_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [send_meeting_invitation_success_schema, error_schema],
}

bulk_send_meeting_invitations_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['status', 'invitations'],
    'properties': {
        'status': {'type': 'string'},
        'invitations': {
            'type': 'array',
            'items': invitation_schema,
        }
    }
}
bulk_send_meeting_invitations_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [bulk_send_meeting_invitations_success_schema, error_schema],
}

get_invitation_success_schema = invitation_schema
get_invitation_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [get_invitation_success_schema, error_schema],
}

respond_to_meeting_invitation_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['status', 'invitation'],
    'properties': {
        'status': {'type': 'string'},
        'invitation': invitation_schema
    }
}
respond_to_meeting_invitation_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [respond_to_meeting_invitation_success_schema, error_schema],
}

get_my_invitations_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'array',
    'items': invitation_schema,
}


def format_invitation(invitation: MeetingInvitation):
    return {
        'id': invitation.id,
        'meeting_id': invitation.meeting_id,
        'status': invitation.status.value,
        'created_at': invitation.created_at,
        'name': invitation.member.name,
        'email': invitation.member.email_address,
    }


verify_email_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['status', 'member'],
    'properties': {
        'status': {'type': 'string'},
        'member': member_info_schema
    }
}

meeting_agenda_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['id', 'meeting_id', 'text', 'updated_at'],
    'properties': {
        'id': {'type': 'number'},
        'meeting_id': {'type': 'number'},
        'text': {'type': 'string'},
        'updated_at': {'type': 'string'},
    }
}

get_meeting_agenda_response_schema = meeting_agenda_schema

edit_meeting_agenda_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['text', 'checkout_timestamp'],
    'properties': {
        'text': {'type': 'string'},
        'checkout_timestamp': {'type': 'string'},
    }
}

edit_meeting_agenda_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['status', 'agenda'],
    'properties': {
        'status': {'type': 'string'},
        'agenda': meeting_agenda_schema
    }
}
edit_meeting_agenda_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [edit_meeting_agenda_success_schema, error_schema],
}


def format_meeting_agenda(agenda: MeetingAgenda):
    return {
        'id': agenda.id,
        'meeting_id': agenda.meeting_id,
        'text': agenda.text,
        'updated_at': agenda.updated_at,
    }
