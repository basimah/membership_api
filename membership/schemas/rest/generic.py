from typing import Callable, Iterable, List, TypeVar

from membership.schemas import Json, JsonObj

T = TypeVar('T')


def format_iterable(
    formatter: Callable[[T], Json]
) -> Callable[[Iterable[T]], List[JsonObj]]:
    def list_formatter(items: Iterable[T]) -> List[JsonObj]:
        return [formatter(item) for item in items]

    return list_formatter


error_schema = {
    'type': 'object',
    'required': ['err', 'status'],
    'properties': {'err': {'type': 'string'}, 'status': {'type': 'string'}},
}
