from sqlalchemy import insert

from membership.database.base import Base, Session, engine


def insert_or_ignore(session: Session, item: Base):
    """Build a statement that inserts a record, or silently ignores if
       it violates a constraint (e.g. already exists)"""

    if engine.dialect.name == 'mysql':
        return insert(item).prefix_with('IGNORE')
    elif engine.dialect.name == 'sqlite':
        # Necessary for tests to pass since they depend on sqlite.
        # `INSERT IGNORE` is not valid sqlite syntax
        return insert(item)
    else:
        raise NotImplementedError(
            "Conflict resolution (insert/ignore) not implemented for this dialect"
        )
