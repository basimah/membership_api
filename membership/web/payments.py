import logging
from operator import itemgetter

import phonenumbers
import stripe
from flask import Blueprint, jsonify, request
from flask.helpers import make_response
from sqlalchemy.exc import IntegrityError

from membership.database.base import Session
from membership.database.models import Identity, IdentityProviders
from membership.models import NoAuthContext
from membership.schemas.rest.payments import (
    create_dues_subscription_request_schema,
    create_dues_subscription_response_schema,
    format_create_member_response,
)
from membership.services import EligibilityService, MemberService, PaymentsService
from membership.services.payments_service import (
    CustomerExistsError,
    PaymentRequiredError,
    InvalidSubscriptionError,
)
from membership.web.util import (
    BadRequest,
    Conflict,
    PaymentRequired,
    RateLimitExceeded,
    ServerError,
    ServiceUnavailable,
    requires_json,
    validate_request,
    validate_response,
)

payments_api = Blueprint('payments_api', __name__)
payments_service = PaymentsService()
eligibility_service = EligibilityService()
member_service = MemberService(eligibility_service=eligibility_service)


class ErrorCodes:
    TOTAL_MISMATCH = "Local and national dues must add up to total amount"
    MEMBER_EMAIL_EXISTS = "Membership already exists for that email"
    PHONE_NUMBER_INVALID = "Invalid phone number"
    ENROLL_MEMBER_ERROR = "Couldn't enroll new member"
    CUSTOMER_EXISTS = "Dues subscription already exists for that email"
    SUBSCRIPTION_PAYMENT_FAILED = "Subscription payment failed"
    PAYMENT_PROCESSOR_BAD_REQUEST = "Bad request to payment processor"
    PAYMENT_PROCESSOR_ERROR = "Issue with payment processor or configuration"
    MEMBER_INTEGRITY_ERROR = "Error when finalizing membership identity"
    SUBSCRIPTION_INVALID = "Invalid subscription plan contents; contact treasurer"
    UNEXPECTED_ERROR = "Something unexpected and bad happened, payment failed"

    @staticmethod
    def subscription_payment_failed_msg(msg: str) -> str:
        return f'Subscription payment failed: {msg}'


@payments_api.route('/payment/dues', methods=['POST'])
@requires_json
@validate_request(create_dues_subscription_request_schema)
@validate_response(create_dues_subscription_response_schema)
def create_dues_subscription():
    email_address = request.json['email_address']
    first_name = request.json.get('first_name', None)
    last_name = request.json.get('last_name', None)
    pronouns = request.json.get('pronouns', None)
    phone_number = request.json.get('phone_number', None)
    address = request.json.get('address', None)
    city = request.json.get('city', None)
    zip_code = request.json.get('zip_code', None)

    payment_info = request.json.get('payment', None)

    if payment_info is not None:
        payment_method_id = payment_info['stripe_payment_method_id']
        local_amount_cents = payment_info['local_amount_cents']
        national_amount_cents = payment_info['national_amount_cents']
        total_amount_cents = payment_info['total_amount_cents']
        recurrence = payment_info['recurrence']

        if local_amount_cents + national_amount_cents != total_amount_cents:
            return BadRequest(ErrorCodes.TOTAL_MISMATCH)

    ctx = NoAuthContext(Session())

    try:
        try:
            member: Member = itemgetter('member')(
                member_service.create_member(
                    ctx.session,
                    first_name=first_name.strip() if first_name else None,
                    last_name=last_name.strip() if last_name else None,
                    email_address=email_address.strip() if email_address else None,
                    pronouns=pronouns.strip() if pronouns else None,
                    phone_number=phone_number.strip() if phone_number else None,
                    address=address.strip() if address else None,
                    city=city.strip() if city else None,
                    zipcode=zip_code,
                    create_member_in_auth0=False,
                    give_chapter_member_role=True,
                    send_member_welcome_email=False,
                    commit_member=False,
                )
            )
        except IntegrityError as e:
            ctx.session.rollback()
            logging.error(ErrorCodes.MEMBER_EMAIL_EXISTS, e)
            return Conflict(ErrorCodes.MEMBER_EMAIL_EXISTS)
        except phonenumbers.NumberParseException as e:
            ctx.session.rollback()
            logging.error(ErrorCodes.PHONE_NUMBER_INVALID, e)
            return BadRequest(ErrorCodes.PHONE_NUMBER_INVALID)
        except Exception as e:
            ctx.session.rollback()
            logging.error(ErrorCodes.ENROLL_MEMBER_ERROR, e)
            return ServerError(ErrorCodes.ENROLL_MEMBER_ERROR)

        if payment_info is None:
            # This is a sponsored dues payment, complete and commit the transaction
            ctx.session.commit()
            return make_response(jsonify(format_create_member_response(member)), 201)

        subscription, customer = payments_service.create_dues_subscription(
            email_address=email_address,
            payment_method_id=payment_method_id,
            local_amount_cents=local_amount_cents,
            national_amount_cents=national_amount_cents,
            recurrence=recurrence,
        )

        collective_dues_identity = Identity(
            provider_name=IdentityProviders.COLLECTIVE_DUES,
            provider_id=subscription.id,
            member_id=member.id,
        )
        ctx.session.add(collective_dues_identity)

        ctx.session.commit()
        return make_response(
            jsonify(format_create_member_response(member, subscription)), 201
        )

    except CustomerExistsError as e:
        ctx.session.rollback()
        logging.error(ErrorCodes.CUSTOMER_EXISTS, e)
        return Conflict(ErrorCodes.CUSTOMER_EXISTS)
    except PaymentRequiredError as e:
        ctx.session.rollback()
        logging.error(ErrorCodes.SUBSCRIPTION_PAYMENT_FAILED, e)
        return PaymentRequired(ErrorCodes.SUBSCRIPTION_PAYMENT_FAILED)
    except stripe.error.CardError as e:
        ctx.session.rollback()
        logging.error(e)
        return PaymentRequired(
            ErrorCodes.subscription_payment_failed_msg(e.error.message)
        )
    except stripe.error.RateLimitError as e:
        ctx.session.rollback()
        logging.error(e)
        return RateLimitExceeded(e.message)
    except stripe.error.InvalidRequestError as e:
        ctx.session.rollback()
        logging.error(ErrorCodes.PAYMENT_PROCESSOR_BAD_REQUEST, e)
        return BadRequest(ErrorCodes.PAYMENT_PROCESSOR_BAD_REQUEST)
    except (
        stripe.error.AuthenticationError,
        stripe.error.APIConnectionError,
        stripe.error.StripeError,
    ) as e:
        ctx.session.rollback()
        logging.error(ErrorCodes.PAYMENT_PROCESSOR_ERROR, e)
        return ServiceUnavailable(ErrorCodes.PAYMENT_PROCESSOR_ERROR)
    except InvalidSubscriptionError as e:
        ctx.session.rollback()
        logging.error(ErrorCodes.SUBSCRIPTION_INVALID, e)
        return ServerError(ErrorCodes.SUBSCRIPTION_INVALID)
    except IntegrityError as e:
        ctx.session.rollback()
        logging.error(ErrorCodes.MEMBER_INTEGRITY_ERROR, e)
        return ServerError(ErrorCodes.MEMBER_INTEGRITY_ERROR)
    except Exception as e:
        ctx.session.rollback()
        logging.error(ErrorCodes.UNEXPECTED_ERROR, e)
        return ServerError(ErrorCodes.UNEXPECTED_ERROR)
    finally:
        ctx.session.close()
