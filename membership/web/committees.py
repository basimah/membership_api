from flask import Blueprint, jsonify, request
from membership.models import AuthContext
from membership.services import CommitteeService
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound


committee_api = Blueprint('committee_api', __name__)
committee_service = CommitteeService()


@committee_api.route('/committee/list', methods=['GET'])
@requires_auth()
def get_committees(ctx: AuthContext):
    return jsonify(committee_service.list(ctx))


@committee_api.route('/committee', methods=['POST'])
@requires_auth('admin')
def add_committee(ctx: AuthContext):
    return jsonify({
        'status': 'success',
        'created': committee_service.create(
            ctx,
            name=request.json['name'],
            provisional=request.json.get('provisional', False),
            admins=request.json.get('admin_list', []),
        )
    })


@committee_api.route('/committee/<int:committee_id>', methods=['GET'])
@requires_auth()
def get_committee(ctx: AuthContext, committee_id: int):
    committee = committee_service.get(ctx, committee_id=committee_id)
    if committee is None:
        return NotFound('Committee id={} does not exist'.format(committee_id))
    return jsonify(committee)


@committee_api.route('/committee/<int:committee_id>/member_request', methods=['POST'])
@requires_auth()
def request_committee_membership(ctx: AuthContext, committee_id: int):
    result = committee_service.request_membership(
            ctx,
            committee_id=committee_id,
            member_name=ctx.requester.name,
            member_email=ctx.requester.email_address
    )

    if result is None:
        return BadRequest('There is no email address associated with this committee')

    return jsonify(
        {
            'status': 'success',
            'data': {
                'email_sent': result
            }
        }
    )
