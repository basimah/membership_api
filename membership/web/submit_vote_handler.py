from typing import Optional  # noqa: F401 ignore used import needed for bullshit

from membership.database.base import Session
from membership.database.models import Election, Member, EligibleVoter, Vote, Ranking
from datetime import datetime, timezone
import string
import secrets
from sqlalchemy.exc import IntegrityError
from enum import Enum


class VotingError(Enum):
    TOO_EARLY = 1
    TOO_LATE = 2
    ALREADY_VOTED = 3
    INELIGIBLE = 4
    UNKNOWN_ELECTION = 5
    VOTE_KEY_RESERVATION_FAILURE = 6


# Returns the vote key for the created vote, or an error code.
def handle_vote(requester: Member, session: Session, election_id: int, rankings):
    election: Election = session.query(Election).get(election_id)
    if not election:
        return VotingError.UNKNOWN_ELECTION

    if election.status == 'final':
        return VotingError.TOO_LATE
    now = datetime.now(timezone.utc)
    if election.voting_begins is not None and election.voting_begins > now:
        return VotingError.TOO_EARLY
    if election.voting_ends is not None and election.voting_ends < now:
        return VotingError.TOO_LATE

    eligible: Optional[EligibleVoter] = session.query(EligibleVoter) \
        .filter_by(member_id=requester.id, election_id=election_id)\
        .with_for_update()\
        .one_or_none()
    if not eligible:
        return VotingError.INELIGIBLE
    if eligible.voted:
        return VotingError.ALREADY_VOTED
    eligible.voted = True

    commit_attempts = 0
    while commit_attempts < 3:
        vote_key = find_vote_key(session, election_id, 6, 6)
        vote = Vote(vote_key=vote_key, election_id=election_id)

        for rank, candidate_id in enumerate(rankings):
            ranking = Ranking(rank=rank, candidate_id=candidate_id)
            vote.ranking.append(ranking)

        # May throw IntegrityError because the find_vote_key method does not guarantee
        # exclusivity against concurrent transactions.
        # That's fine because it'll be a server error, the client can retry.
        session.add(vote)
        try:
            session.commit()
            return vote_key
        except IntegrityError:
            commit_attempts += 1

    return VotingError.VOTE_KEY_RESERVATION_FAILURE


# Attempts to find a vote key that has not yet been claimed by a different vote submission.
# If none can be found within 10 tries, well, that is highly improbable and we throw an exception.
#
# NOTE: This method may still return a vote key which has been claimed by a concurrent transaction.
# We would need to use transaction isolation level SERIALIZABLE to avoid this phantom row problem.
def find_vote_key(session: Session, election_id: int, digits: int, required_digits: int = 0):
    for _ in range(10):
        candidate_key = int(''.join(secrets.choice(string.digits) for i in range(digits)))
        if len(str(candidate_key)) < required_digits:
            continue

        if session.query(Vote)\
                .filter_by(vote_key=candidate_key, election_id=election_id)\
                .one_or_none() is None:
            return candidate_key

    raise Exception('Failed to find a random vote id in ten tries. Think something is wrong.')
