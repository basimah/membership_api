import datetime

from membership.database.base import Session
from membership.database.models import Meeting, MeetingAgenda
from membership.services import ValidationError


class MeetingAgendaService:
    class ErrorCodes:
        UPDATE_CONFLICT = "UPDATE_CONFLICT"

    @staticmethod
    def get_or_create_agenda(*, meeting: Meeting, session: Session) -> MeetingAgenda:
        agenda = meeting.agenda
        if not agenda:
            agenda = MeetingAgenda(meeting_id=meeting.id)
            session.add(agenda)
            session.commit()
        return agenda

    @staticmethod
    def create_or_update_agenda(
        *,
        meeting: Meeting,
        text: str,
        checkout_timestamp_str: str,
        session: Session,
    ) -> MeetingAgenda:
        agenda = meeting.agenda
        if agenda:
            checkout_timestamp = datetime.datetime.fromisoformat(checkout_timestamp_str)
            if checkout_timestamp < agenda.updated_at:
                raise ValidationError(
                    MeetingAgendaService.ErrorCodes.UPDATE_CONFLICT,
                    (
                        'The agenda has been edited since you last loaded it.'
                        ' Please refresh the page to see the latest updates'
                        ' (you may want to save your edits elsewhere first).'
                    )
                )
            agenda.text = text
            agenda.updated_at = datetime.datetime.now().replace(microsecond=0)
        else:
            agenda = MeetingAgenda(meeting_id=meeting.id, text=text)
        session.add(agenda)
        session.commit()
        return agenda
