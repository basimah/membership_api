import re
from typing import List, Optional

import phonenumbers
from phonenumbers import PhoneNumberFormat

from membership.database.base import Session
from membership.database.models import PhoneNumber
from membership.database.statements import insert_or_ignore


class PhoneNumberService:
    def create_number(self, number: str, name: str = "Cell") -> Optional[PhoneNumber]:
        """Creates a phone number with a normalized format."""
        raw_number = re.sub("[(). +-]*", lambda m: "", number)
        if not raw_number:
            return None
        full_phone_number = f"+1 {number}" if len(raw_number) == 10 else number
        formatted_num = phonenumbers.format_number(
            phonenumbers.parse(full_phone_number), PhoneNumberFormat.INTERNATIONAL
        )
        return PhoneNumber(number=formatted_num, name=name)

    def upsert(self, session: Session, member_id: int, number: PhoneNumber) -> None:
        number.member_id = member_id
        self.add_all(session, [number])

    def add_all(self, session: Session, numbers: List[PhoneNumber]) -> None:
        statement = insert_or_ignore(session, PhoneNumber)
        session.execute(
            statement,
            [
                {"member_id": pn.member_id, "number": pn.number, "name": pn.name}
                for pn in numbers
            ],
        )
        session.commit()
