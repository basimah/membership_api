"""Operations related to committees."""

import logging
from typing import Any, Dict, List, Optional, Sequence

from config import EMAIL_CONNECTOR
from membership.database.models import Committee, Email, Member, Role
from membership.models import AuthContext
from membership.schemas.rest.committees import format_committee, format_committee_details
from membership.util.email import get_email_connector, send_committee_request_email

email_connector = get_email_connector(EMAIL_CONNECTOR)


class CommitteeService:
    """Operations related to committees."""

    def list(self, ctx: AuthContext) -> List[Dict[str, Any]]:
        """Returns a formatted list of all committees."""
        committees = ctx.session.query(Committee).all()
        return [format_committee(c, ctx.az) for c in committees]

    def create(
        self,
        ctx: AuthContext,
        name: str,
        provisional: bool = False,
        admins: Sequence[str] = []
    ) -> Dict[str, Any]:
        """Creates a new committee."""
        committee = Committee(name=name)
        ctx.session.add(committee)
        committee.provisional = provisional
        members = ctx.session.query(Member).filter(Member.email_address.in_(admins)).all()
        for member in members:
            role = Role(role='admin', committee=committee, member=member)
            ctx.session.add(role)
        ctx.session.commit()
        return format_committee_details(committee, ctx.az)

    def get(self, ctx: AuthContext, committee_id: int) -> Optional[Dict[str, Any]]:
        """Returns a specific committee."""
        ctx.az.verify_admin(committee_id)
        committee = ctx.session.query(Committee).get(committee_id)
        if committee is None:
            return None
        return format_committee_details(committee, ctx.az)

    def request_membership(
        self,
        ctx: AuthContext,
        committee_id: int,
        member_name: str,
        member_email: str
    ) -> Optional[bool]:
        """Requests membership in a committee for a member."""
        email = ctx.session.query(Email).filter_by(committee_id=committee_id).one_or_none()
        if email is None or email.email_address is None:
            return None

        email_sent = False
        try:
            send_committee_request_email(
                email_connector,
                member_name,
                member_email,
                email.email_address,
            )
            email_sent = True
        except Exception as e:
            logging.error(
                f"Could not send committee request email to committee.id={committee_id}: {e}",
                exc_info=e
            )

        return email_sent
