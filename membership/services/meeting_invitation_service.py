from typing import List

from sqlalchemy import and_

from config import EMAIL_CONNECTOR
from membership.database.base import Session
from membership.database.models import MeetingInvitation, Member, Role, MeetingInvitationStatus
from membership.services import ValidationError, MeetingService
from membership.util.email import send_meeting_invitation_emails, get_email_connector

email_connector = get_email_connector(EMAIL_CONNECTOR)


class MeetingInvitationService:
    class ErrorCodes:
        ALREADY_INVITED = "ALREADY_INVITED"

    @staticmethod
    def send_meeting_invitation(
        *,
        meeting_id: int,
        member_id: int,
        session: Session,
    ) -> MeetingInvitation:
        existing_invitation = (
            session.query(MeetingInvitation)
            .filter_by(meeting_id=meeting_id, member_id=member_id)
            .one_or_none()
        )
        if existing_invitation:
            raise ValidationError(
                MeetingInvitationService.ErrorCodes.ALREADY_INVITED,
                f'Member {member_id} has already been invited to meeting {meeting_id}'
            )
        invitation = MeetingInvitation(meeting_id=meeting_id, member_id=member_id)
        session.add(invitation)
        session.commit()
        if invitation.meeting.published:
            send_meeting_invitation_emails(email_connector, [invitation])
            invitation.status = MeetingInvitationStatus.NO_RESPONSE
            session.commit()
        return invitation

    @staticmethod
    def bulk_send_meeting_invitations(
        *,
        meeting_id: int,
        committee_id: int,
        session: Session,
    ) -> List[MeetingInvitation]:
        uninvited_member_ids = list(m.id for m in (
            session.query(Member)
            .with_entities(Member.id)
            .distinct(Member.id)
            .join(Role, Member.id == Role.member_id)
            .outerjoin(MeetingInvitation, and_(
                MeetingInvitation.meeting_id == meeting_id,
                Member.id == MeetingInvitation.member_id,
            ))
            .filter(Role.committee_id == committee_id)
            .filter(MeetingInvitation.id.is_(None))
            .all()
        ))
        invitations = []
        for member_id in uninvited_member_ids:
            invitation = MeetingInvitation(meeting_id=meeting_id, member_id=member_id)
            session.add(invitation)
            invitations.append(invitation)
        session.commit()
        meeting = MeetingService().find_meeting_by_id(str(meeting_id), session)
        if meeting.published:
            send_meeting_invitation_emails(email_connector, invitations)
            for invitation in invitations:
                invitation.status = MeetingInvitationStatus.NO_RESPONSE
            session.commit()
        return invitations

    @staticmethod
    def send_pending_invitations(*, meeting_id: int, session: Session) -> List[MeetingInvitation]:
        pending_invitations = (
            session.query(MeetingInvitation)
            .filter(MeetingInvitation.meeting_id == meeting_id)
            .filter(MeetingInvitation.status == MeetingInvitationStatus.SEND_PENDING)
            .all()
        )
        send_meeting_invitation_emails(email_connector, pending_invitations)
        for invitation in pending_invitations:
            invitation.status = MeetingInvitationStatus.NO_RESPONSE
        session.commit()
        return pending_invitations
