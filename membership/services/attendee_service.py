from secrets import token_urlsafe
from typing import Optional, Union

from membership.database.base import Session
from membership.database.models import (
    Attendee,
    AttendeeEmailVerifyToken,
    Meeting,
    Member,
)
from membership.services.errors import ValidationError


class AttendeeService:
    class ErrorCodes:
        ALREADY_ATTENDED = "ALREADY_ATTENDED"

    def attend_meeting_with_short_id(
        self, member: Union[Member, int, str], short_id: int, session: Session
    ) -> Optional[Meeting]:
        meeting: Meeting = session.query(Meeting).filter_by(
            short_id=short_id
        ).one_or_none()

        if not meeting:
            return None

        try:
            self.attend_meeting(member, meeting, session)
        except ValidationError as e:
            if e.key == AttendeeService.ErrorCodes.ALREADY_ATTENDED:
                raise ValidationError(
                    AttendeeService.ErrorCodes.ALREADY_ATTENDED,
                    "Member has already attended meeting (meeting code: {})".format(
                        short_id
                    ),
                )
            else:
                raise e

        return meeting

    def attend_meeting(
        self,
        member: Union[Member, int, str],
        meeting: Union[Meeting, int, str],
        session: Session,
        verified: bool = False,
        return_none_on_error: bool = False,
    ) -> Optional[Attendee]:
        if type(member) in [int, str]:
            member: Member = session.query(Member).get(member)

        if type(meeting) in [int, str]:
            meeting: Meeting = session.query(Meeting).get(meeting)

        if (
            session.query(Attendee).filter_by(meeting=meeting, member=member).count()
            == 0
        ):
            attendee = Attendee(meeting=meeting, member=member, verified=verified)
            session.add(attendee)
            session.commit()
            return attendee
        elif return_none_on_error:
            return None
        else:
            raise ValidationError(
                AttendeeService.ErrorCodes.ALREADY_ATTENDED,
                "Member has already attended meeting (id: {})".format(meeting.id),
            )

    def retrieve_attendee(
        self,
        session: Session,
        member: Optional[Union[Member, int, str]] = None,
        meeting: Optional[Union[Meeting, int, str]] = None,
        attendee: Optional[Union[Attendee, int, str]] = None,
    ) -> Attendee:
        if type(member) in [int, str]:
            member: Member = session.query(Member).get(member)

        if type(meeting) in [int, str]:
            meeting: Meeting = session.query(Meeting).get(meeting)

        if type(attendee) in [int, str]:
            attendee: Attendee = session.query(Attendee).get(attendee)

        if attendee == None:
            attendee = (
                session.query(Attendee).filter_by(member=member, meeting=meeting).one()
            )
        elif member != None or meeting != None:
            raise ValueError('Must provide a Member and Meeting, or an Attendee')

        return attendee

    def create_attendee_token(
        self,
        *,
        session: Session,
        member: Member,
        attendee: Attendee,
        email_address_override: Optional[str] = None,
        commit: bool = False
    ) -> AttendeeEmailVerifyToken:
        email_address = (
            email_address_override
            if email_address_override is not None
            else member.email_address
        )

        token = AttendeeEmailVerifyToken(
            member=member,
            attendee=attendee,
            email_address=email_address,
            token=token_urlsafe(20),
        )
        session.add(token)
        session.flush()

        if commit:
            session.commit()

        return token
