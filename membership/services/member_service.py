import logging
from typing import Any, Callable, Dict, List, Optional, Tuple

import phonenumbers
from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError

from config import (
    EMAIL_CONNECTOR,
    USE_EMAIL,
    DISABLE_CREATE_AUTH0_USER,
    DISABLE_WELCOME_EMAIL
)
from membership.database.base import Base, Session
from membership.database.models import (
    AdditionalEmailAddress,
    Attendee,
    EligibleVoter,
    Identity,
    Interest,
    Member,
    NationalMembershipData,
    PhoneNumber,
    ProxyToken,
    Role,
    MeetingInvitation)
from membership.models import (
    MemberQueryResult,
    MergeMemberFetchResult,
    MergeMemberFlaggedEligibleVoter,
    MergeMemberFlaggedProxyToken,
    MergeMemberFlags,
)
from membership.services.eligibility import EligibilityService
from membership.services.search_service import process_search_query
from membership.services.phone_number_service import (
    PhoneNumberService
)
from membership.util.email import get_email_connector, send_welcome_email
from membership.web.auth import create_auth0_user, CreateAuth0UserError


email_connector = get_email_connector(EMAIL_CONNECTOR)
phone_number_service = PhoneNumberService()


class MemberService:
    def __init__(self, eligibility_service: EligibilityService):
        self.eligibility_service = eligibility_service

    def create_member(
        self,
        session: Session,
        *,
        first_name: Optional[str],
        last_name: Optional[str],
        email_address: Optional[str],
        pronouns: Optional[str] = None,
        phone_number: Optional[str] = None,
        address: Optional[str] = None,
        city: Optional[str] = None,
        zipcode: Optional[str] = None,
        create_member_in_auth0: bool,
        give_chapter_member_role: bool,
        send_member_welcome_email: bool,
        commit_member: bool = True
    ) -> Dict[str, Any]:
        member = Member(
            first_name=first_name,
            last_name=last_name,
            email_address=email_address,
            normalized_email=Member.normalize_email(email_address),
            pronouns=pronouns,
            address=address,
            city=city,
            zipcode=zipcode
        )
        session.add(member)

        try:
            # Do this to get the assigned member_id for phone numbers
            session.flush()
        except IntegrityError as e:
            session.rollback()
            raise e

        if phone_number is not None:
            phone_number_entry = phone_number_service.create_number(phone_number)

            if phone_number_entry is not None:
                phone_number_entry.member_id = member.id
                session.add(phone_number_entry)
            else:
                raise phonenumbers.NumberParseException()

        try:
            session.flush()
        except IntegrityError as e:
            session.rollback()
            raise e

        try:
            verify_url = None
            if create_member_in_auth0 and not DISABLE_CREATE_AUTH0_USER:
                try:
                    verify_url = create_auth0_user(member.email_address)
                except CreateAuth0UserError as e:
                    logging.error(f"Could not create Auth0 user for {member.email_address}: {e}")
                    raise e

            if give_chapter_member_role:
                role = Role(member_id=member.id, role='member', committee_id=None)
                session.add(role)
                session.flush()

            email_sent = False
            if (
                verify_url and
                send_member_welcome_email and
                USE_EMAIL and
                not DISABLE_WELCOME_EMAIL
            ):
                session.refresh(member)
                try:
                    send_welcome_email(email_connector, member, verify_url)
                    email_sent = True
                except Exception as e:
                    logging.error(f"Could not send welcome email to member.id={member.id}: {e}")

            if commit_member:
                session.commit()
            else:
                session.flush()

            return {
                'member': member,
                'verify_url': verify_url,
                'email_sent': email_sent
            }
        except Exception as e:
            session.rollback()
            raise e

    def find_by_email(self, email: str, session: Session) -> Optional[Member]:
        member = (
            session.query(Member)
            .filter(
                or_(
                    Member.email_address == email,
                    Member.normalized_email == Member.normalize_email(email)
                )
            )
            .one_or_none()
        )

        addl_email = None
        if member is None:
            addl_email = (
                session.query(AdditionalEmailAddress)
                .filter_by(email_address=email)
                .one_or_none()
            )

            if addl_email is not None:
                member = addl_email.member
            else:
                member = None

        return member

    def all(self, session: Session) -> MemberQueryResult:
        members = self._all_members(session)
        next_cursor = self._get_cursor_from_members(members, last_cursor=None)
        return self._as_eligible_members_result(members, next_cursor, False, session)

    def query(self,
              session: Session,
              cursor: Optional[str] = None,
              page_size: Optional[int] = None,
              query_str: Optional[str] = None) -> MemberQueryResult:

        min_id = int(cursor) + 1 if cursor else 0

        # Query for 1 more so we know if we have more
        limit = page_size + 1 if page_size else None
        members = self._query_members(session=session,
                                      min_id=min_id,
                                      limit=limit,
                                      query_str=query_str)

        has_more = False
        if page_size is not None and len(members) == page_size + 1:
            has_more = True
            members = members[:-1]  # remove extra element

        next_cursor = self._get_cursor_from_members(members, last_cursor=cursor)
        return self._as_eligible_members_result(members, next_cursor, has_more, session)

    def _all_members(self, session: Session) -> List[Member]:
        query = session \
                .query(Member) \
                .all()

        return list(query)

    def _query_members(self,
                       session: Session,
                       min_id: int,
                       limit: Optional[int] = None,
                       query_str: Optional[str] = None) -> List[Member]:
        members_query = session.query(Member) \
                               .order_by(Member.id) \
                               .filter(Member.id >= min_id)

        processed_query = process_search_query(members_query, query_str)

        if limit:
            processed_query = processed_query.limit(limit)
        result = list(processed_query.all())
        for member in result:
            assert member.id >= int(min_id)
        return result

    def _as_eligible_members_result(self, members: List[Member],
                                    next_cursor: str,
                                    has_more: bool,
                                    session: Session) -> MemberQueryResult:
        members_with_eligibility_to_vote = self.eligibility_service.members_as_eligible_to_vote(
            session,
            members
        )

        return MemberQueryResult(
            members=members_with_eligibility_to_vote,
            cursor=next_cursor,
            has_more=has_more,
        )

    def _get_cursor_from_members(self, members: List[Member], last_cursor: Optional[str]) -> str:
        if len(members) > 0:
            return str(members[-1].id)
        elif last_cursor:
            return last_cursor
        else:
            return str(-1)

    def check_for_merge_member_flags(
            self,
            member_to_keep_id: int,
            member_to_remove_id: int,
            fetch_data: MergeMemberFetchResult) -> Optional[MergeMemberFlags]:
        """
        Checks for any sensitive deletes that should be surfaced to the user before forcing the
        merge. These cases will be flagged:
            - There is a duplicate EligibleVoter record, as removing this record would alter vote
              counts.
            - There is a duplicate ProxyToken record, as removing this record would alter vote
              counts.
        """
        eligible_voter_dups = self.find_dups_and_non_dups(
            fetch_data.eligible_voters_to_keep,
            fetch_data.eligible_voters_to_remove,
            lambda x, y: x.election_id == y.election_id)[0]
        proxy_token_dups = self.find_dups_and_non_dups(
            fetch_data.proxy_tokens_to_keep,
            fetch_data.proxy_tokens_to_remove,
            lambda x, y: x.meeting_id == y.meeting_id)[0]

        flagged_eligible_voters = \
            [self.flagged_eligible_voters(pair[0], pair[1]) for pair in eligible_voter_dups]
        flagged_proxy_tokens = [self.flagged_proxy_token(pair[1]) for pair in proxy_token_dups]

        if any(flagged_proxy_tokens) or any(flagged_eligible_voters):
            return MergeMemberFlags(flagged_eligible_voters, flagged_proxy_tokens)
        else:
            return None

    def find_dups_and_non_dups(
            self,
            to_keep: List[Any],
            to_remove: List[Any],
            is_duplicate_pred: Callable[[EligibleVoter, EligibleVoter], bool]
    ) -> Tuple[List[Tuple[EligibleVoter, EligibleVoter]], List[EligibleVoter]]:
        """
        An abstraction for identifying duplicate records for any given table.

        Retuns a tuple of collections. The left is a list of tuples representing each (keep, remove)
        pair.The right is a list of any "to_remove" records which are not duplicates.
        """
        dups = []
        logging.info(str(list(map(lambda x: x.id, to_keep))))
        logging.info(str(list(map(lambda x: x.id, to_remove))))
        for k in to_keep:
            for r in to_remove:
                if (is_duplicate_pred(k, r)):
                    dups.append((k, r))

        non_dups = []
        for r in to_remove:
            matches_dup = filter(lambda d: r.id == d[1].id, dups)
            if not any(matches_dup):
                non_dups.append(r)
        logging.info(str(list(map(lambda x: (x[0].id, x[1].id), dups))))
        logging.info(str(list(map(lambda x: x.id, non_dups))))

        return (dups, non_dups)

    def flagged_eligible_voters(
            self,
            eligible_voters_to_keep: EligibleVoter,
            eligible_voters_to_remove: EligibleVoter) -> MergeMemberFlaggedEligibleVoter:
        return MergeMemberFlaggedEligibleVoter(
            eligible_voters_to_keep.election_id,
            eligible_voters_to_keep.voted,
            eligible_voters_to_remove.voted)

    def flagged_proxy_token(
            self,
            proxy_token: ProxyToken) -> MergeMemberFlaggedProxyToken:
        return MergeMemberFlaggedProxyToken(
            proxy_token.member_id,
            proxy_token.meeting_id,
            proxy_token.receiving_member_id,
            proxy_token.state)

    def interest_pred(x: Interest, y: Interest):
        return x.topic_id == y.topic_id

    def phone_number_pred(x: PhoneNumber, y: PhoneNumber):
        return x.number == y.number

    def role_pred(x: Role, y: Role):
        return x.role == y.role and x.committee_id == y.committee_id

    def eligible_pred(x: EligibleVoter, y: EligibleVoter):
        return x.election_id == y.election_id

    def perform_merge_for_all_tables(
            self,
            member_id_to_keep: int,
            member_id_to_remove, fetch_data: MergeMemberFetchResult, session: Session):
        """
        Executes the merge logic. All statements wrapped in a transaction and will be rolled back if
        any fail (if this happens, it's probably a bug).

        Each table has unique logic:
            Interests
                - duplicate if: "topic_id" matches for both records
                - if a duplicate record has an older 'created_date', update the record being kept to
                  that date
                - delete duplicate record
                - update member_id for non-duplicate records
            PhoneNumbers
                - duplicate if: "number" matches for both records
                - same merge logic as Interests
            Roles
                - duplicate if: "role" and "committee_id" match for both records
                - same merge logic as Interests
            EligibleVoters
                - duplicate if: "election_id" matches for both records
                - if a duplicate record is marked as having "voted", update kept record as
                  "voted" = True (assuming it wasn't already)
                - delete duplicate record
                - update member_id for non-duplicate records
            ProxyTokens
                - duplicate if: "meeting_id" matches for both records
                - delete duplicate record
                - update member_id for non-duplicate records
                - if any record not involved in the merge has
                  "receiving_member_id" == "member_id_to_remove", update value to
                  "member_id_to_keep"
            Attendees
                - duplicate if: "meeting_id" matches for both records
                - delete duplicate record
                - update member_id for non-duplicate records
            Identities
                - duplicate if: Never, provider_id is part of the primary_key, so no two records can
                                be the same identity. TODO: verify if providers can be duplicates.
                - delete duplicate record
                - update member_id for non-duplicate records
            NationalMembershipData
                - duplicate if: Never, ak_id is part of primary key, so no two records can be the
                                same national member. TODO: verify if national members can be
                                duplicates.
                - delete duplicate record
                - update member_id for non-duplicate records
        """
        num_updates = 0
        num_deletions = 0

        try:
            num_updates += self.update_date_field_if_newer(
                fetch_data.interests_to_keep,
                fetch_data.interests_to_remove,
                MemberService.interest_pred,
                'created_date',
                session)
            num_updates += self.update_date_field_if_newer(
                fetch_data.phone_numbers_to_keep,
                fetch_data.phone_numbers_to_remove,
                MemberService.phone_number_pred,
                'date_imported',
                session)
            num_updates += self.update_date_field_if_newer(
                fetch_data.roles_to_keep,
                fetch_data.roles_to_remove,
                MemberService.role_pred,
                'date_created',
                session)
            num_updates += self.update_has_voted_if_dup_voted(
                fetch_data.eligible_voters_to_keep,
                fetch_data.eligible_voters_to_remove,
                MemberService.eligible_pred,
                session)

            logging.info(str((num_updates, num_deletions)))
            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.interests_to_keep,
                    fetch_data.interests_to_remove,
                    MemberService.interest_pred,
                    session),
                num_updates,
                num_deletions)
            logging.info(str((num_updates, num_deletions)))

            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.phone_numbers_to_keep,
                    fetch_data.phone_numbers_to_remove,
                    MemberService.phone_number_pred,
                    session),
                num_updates,
                num_deletions)
            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.roles_to_keep,
                    fetch_data.roles_to_remove,
                    MemberService.role_pred,
                    session),
                num_updates,
                num_deletions)
            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.eligible_voters_to_keep,
                    fetch_data.eligible_voters_to_remove,
                    MemberService.eligible_pred,
                    session),
                num_updates,
                num_deletions)
            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.attendees_to_keep,
                    fetch_data.attendees_to_remove,
                    lambda x, y: x.meeting_id == y.meeting_id, session),
                num_updates,
                num_deletions)
            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.invitations_to_keep,
                    fetch_data.invitations_to_remove,
                    lambda x, y: x.meeting_id == y.meeting_id, session),
                num_updates,
                num_deletions)

            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.proxy_tokens_to_keep,
                    fetch_data.proxy_tokens_to_remove,
                    lambda x, y: x.meeting_id == y.meeting_id, session),
                num_updates,
                num_deletions)
            num_updates += self.update_additional_proxy_tokens(
                member_id_to_keep,
                fetch_data.additional_proxy_tokens_to_update,
                session)

            # Note, identities cannot be duplicated because provider_id is part of the primary key.
            # All records will have member_id updated to "member_id_to_keep".
            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.identities_to_keep,
                    fetch_data.identities_to_remove,
                    lambda x, y: False, session),
                num_updates,
                num_deletions)
            # Note, membership_usa (national members) cannot be duplicated because ak_id is part of
            # the primary key. All records will have member_id updated to "member_id_to_keep".
            (num_updates, num_deletions) = self.add_count(
                self.remove_dups_and_update_non_dup_ids(
                    member_id_to_keep,
                    fetch_data.national_members_to_keep,
                    fetch_data.national_members_to_remove,
                    lambda x, y: False, session),
                num_updates,
                num_deletions)

            session.query(Member).filter(Member.id == member_id_to_remove).delete()
            num_deletions += 1

            session.commit()
            return (num_updates, num_deletions)
        except:
            session.rollback()
            raise

    def add_count(
            self,
            new_changes: Tuple[int, int],
            total_updates: int,
            total_deletions: int) -> Tuple[int, int]:
        return (new_changes[0] + total_updates, new_changes[1] + total_deletions)

    def remove_dups_and_update_non_dup_ids(
            self,
            member_id_to_keep: int,
            keep_data: List[Any],
            remove_data: List[Any],
            is_duplicate_pred: Callable[[Any, Any], bool], session: Session):
        """
        Performs the merge of records for two members on a single table. The merge will behave as
        follows:
            - Any record within "remove_data" that duplicates a record in "keep_date" base on the
              provided predicate will be deleted.
            - Any record within "remove_data" that does not duplicate another record will have its
              member_id changed to the "member_id_to_keep"

        Paramaters:
        memeber_id_to_keep (int): The member_id being merged into. These records will remain after
                                  the merge.
        keep_data (List[Any]): These items represent table entries for each row matching
                               member_id = member_id_to_keep
        remove_data (List[Any]): These items represent table entries for each
                                 matching member_id = the id marked for removal
        is_duplicate_pred: Predicate that determines is a record marked for removal would duplicate
                           a record marked to be kept if its member_id was updated.
        """
        (dups, non_dups) = self.find_dups_and_non_dups(keep_data, remove_data, is_duplicate_pred)
        for pair in dups:
            session.delete(pair[1])
        for record in non_dups:
            record.member_id = member_id_to_keep

        return (len(non_dups), len(dups))

    def update_date_field_if_newer(
            self,
            keep_data: List[Any],
            remove_data: List[Any],
            is_duplicate_pred: Callable[[Any, Any], bool],
            date_attr: str, session: Session) -> int:
        num_updates = 0

        (dups, non_dups) = self.find_dups_and_non_dups(keep_data, remove_data, is_duplicate_pred)
        for pair in dups:
            keep_record = pair[0]
            keep_date = getattr(keep_record, date_attr)
            remove_date = getattr(pair[1], date_attr)
            if keep_date and remove_date and keep_date > remove_date:
                num_updates += 1
                setattr(keep_record, date_attr, remove_date)

        return num_updates

    def update_has_voted_if_dup_voted(
            self,
            keep_data: List[Any],
            remove_data: List[Any],
            is_duplicate_pred: Callable[[Any, Any], bool], session: Session) -> int:
        num_updates = 0

        (dups, non_dups) = self.find_dups_and_non_dups(keep_data, remove_data, is_duplicate_pred)
        for pair in dups:
            keep_record = pair[0]
            remove_record = pair[1]
            if not keep_record.voted and remove_record.voted:
                num_updates += 1
                keep_record.voted = True

        return num_updates

    def update_additional_proxy_tokens(
            self,
            member_id_to_keep: int,
            additional_data: List[ProxyToken],
            session: Session):
        """
        Updates any existing proxy token record where the "receiving_member_id" is equal to the
        member_id being removed.

        member_id_to_keep (int): The member_id being merged into. These records will remain after
                                 the merge.
        remove_data (List[ProxyToken]): These items represent table entries for each row matching
                                        member_id = the id marked for removal.
        additional_data (List[ProxyToken]): A list of records where receiving_member_id == the id
                                            being removed.
        """
        for r in additional_data:
            r.receiving_member_id = member_id_to_keep
        return len(additional_data)

    def merge_member_fetch_data(
            self,
            member_to_keep_id: int,
            member_to_remove_id: int,
            session: Session) -> MergeMemberFetchResult:
        """
        Fetches all data from the database needed to validate or carry out a member merge operation.

        Note, each table will be selected from in a separate database call. This inefficiency should
        be ok since these services should be called infrequently and only by admins.

        Parameters:
        member_to_keep_id (int): The member_id of the user that will be merged into (i.e. will
                                 remain after merge operation is complete)
        member_to_remove_id (int): The member_id of the user that will be merged (i.e. will no
                                   longer exist after merge operation is complete)

        Returns:
        MergeMemberFetchResult: Storage of all the records that are fetched, split into seperate
        lists based on member_id == keep_id or member_id == remove_id.
        """
        member_to_keep = session.query(Member).filter(Member.id == member_to_keep_id).first()
        member_to_remove = session.query(Member).filter(Member.id == member_to_remove_id).first()
        (attendees_to_keep, attendees_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, Attendee, session)
        (eligible_voters_to_keep, eligible_voters_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, EligibleVoter, session)
        (identities_to_keep, identities_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, Identity, session)
        (interests_to_keep, interests_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, Interest, session)
        (national_members_to_keep, national_members_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, NationalMembershipData, session)
        (phone_numbers_to_keep, phone_numbers_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, PhoneNumber, session)
        (proxy_tokens_to_keep, proxy_tokens_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, ProxyToken, session)
        additional_proxy_tokens_to_update = self.merge_fetch_additional_proxy_token_date(
            member_to_remove_id, session)
        (roles_to_keep, roles_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, Role, session)
        (invitations_to_keep, invitations_to_remove) = self.merge_fetch_from_table(
            member_to_keep_id, member_to_remove_id, MeetingInvitation, session)

        return MergeMemberFetchResult(
            member_to_keep,
            member_to_remove,
            attendees_to_keep,
            attendees_to_remove,
            eligible_voters_to_keep,
            eligible_voters_to_remove,
            identities_to_keep,
            identities_to_remove,
            interests_to_keep,
            interests_to_remove,
            national_members_to_keep,
            national_members_to_remove,
            phone_numbers_to_keep,
            phone_numbers_to_remove,
            proxy_tokens_to_keep,
            proxy_tokens_to_remove,
            additional_proxy_tokens_to_update,
            roles_to_keep,
            roles_to_remove,
            invitations_to_keep,
            invitations_to_remove,
        )

    def merge_fetch_from_table(
            self,
            member_to_keep_id: int,
            member_to_remove_id: int,
            table: Base,
            session: Session) -> Tuple[List[Base], List[Base]]:
        """
        Runs a select query on the given table where member_id matches on of the provided id params.

        Parameters:
        member_to_keep_id (int): The member_id of the user that will be merged into (i.e. will
                                 remain after merge operation is complete)
        member_to_remove_id (int): The member_id of the user that will be merged (i.e. will no
                                   longer exist after merge operation is complete)

        Returns: tuple of records: Each tuple element is a list of records matching one of the
                                   provided ids (keep_id on left, remove_id on right).
        """
        results = session.query(table) \
            .filter(table.member_id.in_([member_to_keep_id, member_to_remove_id])) \
            .all()
        to_keep = list(filter(lambda x: x.member_id == member_to_keep_id, results))
        to_remove = list(filter(lambda x: x.member_id == member_to_remove_id, results))

        return (to_keep, to_remove)

    def merge_fetch_additional_proxy_token_date(self, member_to_remove_id: int, session: Session):
        additional_data = session.query(ProxyToken) \
            .filter(ProxyToken.receiving_member_id == member_to_remove_id) \
            .all()
        return additional_data
