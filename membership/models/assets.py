from membership.database.models import Asset


class ResolvedAsset:
    def __init__(self, asset: Asset, view_url: str):
        self.asset = asset
        self.view_url = view_url
