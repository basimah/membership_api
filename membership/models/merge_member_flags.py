from membership.models import MergeMemberFlaggedEligibleVoter
from membership.models import MergeMemberFlaggedProxyToken
from typing import List


class MergeMemberFlags:

    def __init__(
            self,
            flagged_eligible_voters: List[MergeMemberFlaggedEligibleVoter],
            flagged_proxy_tokens: List[MergeMemberFlaggedProxyToken]):
        self.flagged_eligible_voters = flagged_eligible_voters
        self.flagged_proxy_tokens = flagged_proxy_tokens
