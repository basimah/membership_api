#!/usr/bin/env python3

import argparse
import csv
import io  # NOQA: F401
import itertools
import re
import sys
from collections import defaultdict
from pprint import pprint
from typing import DefaultDict  # NOQA: F401
from typing import Set  # NOQA: F401
from typing import TextIO  # NOQA: F401
from typing import cast  # NOQA: F401
from typing import (IO, Any, Callable, Dict, Iterable, Iterator, List,
                    Optional, Tuple, TypeVar)

import phonenumbers
from overrides import overrides
from phonenumbers import NumberParseException, PhoneNumberFormat
from sqlalchemy import and_, func, or_
from sqlalchemy.exc import IntegrityError, InvalidRequestError, SQLAlchemyError

from jobs import ConfirmAction, ForceConfirmAction, Job
from membership.database.base import Base, Session
from membership.database.models import (AdditionalEmailAddress, Committee,
                                        Identity, IdentityProviders, Member,
                                        PhoneNumber, Role)
from membership.services import (AdditionalEmailAddressService,
                                 PhoneNumberService)
from membership.services.national import IdentityService

BRANCHES = set(["Lower Peninsula Branch", "San Jose Branch"])


class MasterListKeys:
    SV_DSA_ID = 'SV DSA ID'
    PREFERRED_NAME = 'Preferred Name'
    FIRST_NAME = 'First Name'
    LAST_NAME = 'Last Name'
    EMAIL_1 = 'Email'
    EMAIL_2 = 'Email 2'
    EMAIL_3 = 'Email 3'
    EMAIL_4 = 'Email 4'
    ZIP = 'Zip Code'
    CITY = 'City'
    PHONE_1 = 'Phone'
    PHONE_2 = 'Phone 2'
    PHONE_3 = 'Phone 3'
    COMMS_PREFERENCE = 'Comms Preference'
    DO_NOT_EMAIL = 'Mailchimp Status'
    NOTES = 'Notes'
    BRANCH = 'Branch'


# typedef
Row = Dict[str, str]


def fields_of(table: Base, model, excluding: Iterable[str] = ('id',)) -> dict:
    exclude_set = set(excluding)
    return {
        column: getattr(model, column)
        for column in table.__table__.columns.keys()
        if column not in exclude_set
    }


def refresh_all(objects: Iterable[object], session: Session) -> None:
    session.flush()
    for obj in objects:
        try:
            session.refresh(obj)
        except InvalidRequestError:
            pass


class ImportMasterList(Job):
    """
    Imports the SV DSA Master List without sending any emails / invites on auth0.

    !!!
    This is intended as a one-time import. It _should_ be idempotent but I
    haven't tested that yet.
    !!!

    Gracefully handles?

    New members:
        Inserts Member with
        Adds identities
        Adds member role for branches
        Adds phone numbers
        Adds email addresses

    Creates branch groups

    Matches existing member on normalized email or first_name + last_name
    """

    description = 'Upload SV DSA master list'
    keys = MasterListKeys
    identities = IdentityService()
    phone_numbers = PhoneNumberService()
    addl_email_addresses = AdditionalEmailAddressService()

    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            'input', nargs='?', type=argparse.FileType('r'), default=sys.stdin
        )
        parser.add_argument(
            '-o', '--output', type=argparse.FileType('w'), default=sys.stdout
        )
        parser.add_argument('-f', '--force', action='store_true')

    @overrides
    def run(self, config: dict) -> None:
        print("Import Master List script")
        outfile: IO[str] = config.get('output', sys.stdout)
        row_iter: Optional[TextIO] = config.get('input')
        if row_iter is None:
            raise ValueError(
                f"config['input'] must be an iterator over a CSV file, received None "
                f"(provided keys: {config.keys()})"
            )
        reader = csv.DictReader(row_iter)
        results = self.import_all(
            iter(reader),
            out=outfile,
            confirm_action=ForceConfirmAction if config.get('force') else ConfirmAction,
        )
        print(f"\n{results}", file=outfile)

    class Results:
        def __init__(self, src: Dict[str, Any] = {}, **kwargs):
            results = src or kwargs
            self.members_created: int = results.get('members_created', 0)
            self.members_updated: int = results.get('members_updated', 0)
            self.memberships_created: int = results.get('memberships_created', 0)
            self.memberships_updated: int = results.get('memberships_updated', 0)
            self.member_roles_added: int = results.get('member_roles_added', 0)
            self.identities_created: int = results.get('identities_created', 0)
            self.phone_numbers_created: int = results.get('phone_numbers_created', 0)
            self.email_addresses_created: int = results.get(
                'email_addresses_created', 0
            )
            self.errors: int = results.get('errors', 0)
            self.processed: int = results.get('processed', 0)
            self.excluded: int = results.get('excluded', 0)
            self.total: int = results.get('total', 0)

        def __str__(self):
            return (
                f"Members Created: {self.members_created}\n"
                f"Members Updated: {self.members_updated}\n"
                f"Memberships Created: {self.memberships_created}\n"
                f"Memberships Updated: {self.memberships_updated}\n"
                f"Member Roles Added: {self.member_roles_added}\n"
                f"Identities Created: {self.identities_created}\n"
                f"Phone Numbers Created: {self.phone_numbers_created}\n"
                f"Email Addresses Created: {self.email_addresses_created}\n"
                f"Errors: {self.errors}\n"
                f"Processed: {self.processed}\n"
                f"Excluded: {self.excluded}\n"
                f"Total: {self.total}\n"
            )

        def __repr__(self):
            return f"ImportNationalMembership.Results({repr(vars(self))})"

        def __copy__(self):
            newone = type(self)()
            newone.__dict__.update(self.__dict__)
            return newone

        def __deepcopy__(self, memodict={}):
            return self.__copy__()

    def _debug_row(self, row: Dict[str, str]) -> Dict[str, str]:
        return {
            k: v
            for k, v in row.items()
            if k
            in {
                self.keys.FIRST_NAME,
                self.keys.LAST_NAME,
                self.keys.EMAIL_1,
                self.keys.COMMS_PREFERENCE,
                self.keys.PHONE_1,
            }
        }

    def _debug_member(self, member: Member) -> Dict[str, str]:
        return {
            'first_name': member.first_name,
            'last_name': member.last_name,
            'email_address': member.email_address,
        }

    def find_existing_members(
        self, row: Row, session: Session
    ) -> List[Tuple[Member, bool]]:
        """
        Attempts to find an existing member using the given normalized_email, first_name, and
        last_name.

        :param row:
        :param session:
        :return: List of matching member objects and whether these members have the 'member' role
        """
        try:
            email = row.get(self.keys.EMAIL_1, '').strip()
            first_name = row.get(self.keys.FIRST_NAME, '').strip()
            last_name = row.get(self.keys.LAST_NAME, '').strip()
            preferred_name = row.get(self.keys.PREFERRED_NAME, '').strip()
            preferred_first_name = preferred_name or first_name
            clauses = []

            if email:
                normalized_email = Member.normalize_email(email)
                clauses.append(
                    or_(
                        Member.normalized_email == normalized_email,
                        func.lower(Member.email_address) == email.lower(),
                    )
                )
            elif preferred_first_name and last_name:
                clauses.append(
                    and_(
                        func.lower(Member.first_name) == preferred_first_name.lower(),
                        func.lower(Member.last_name) == last_name.lower(),
                    )
                )
            elif preferred_first_name:
                clauses.append(
                    func.lower(Member.first_name) == preferred_first_name.lower()
                )

            # this is necessary because if we are given no criteria, we should return an
            # empty list instead of every member in the database.
            if not clauses:
                return []
            found = session.query(Member).filter(or_(*clauses)).all()
            return [
                (
                    m,
                    any(
                        r
                        for r in m.roles
                        if r.role == 'member' and r.committee_id is None
                    ),
                )
                for m in found
                if (not m.email_address or not m.email_address.startswith('DELETE'))
            ]
        except SQLAlchemyError as e:
            print(f"ERROR: {e}")
            return []

    def extract_phone_numbers(self, row: Dict[str, str]) -> List[PhoneNumber]:
        # TODO: Replace with a call to PhoneNumberService
        def format_phone(num: str) -> Optional[str]:
            raw_num = re.sub('[(). +-]*', lambda m: '', num)
            if not raw_num:
                return None
            full_phone_number = (
                f"+1 {num}"
                if len(raw_num) == 10 or (len(raw_num) == 11 and raw_num[0] == '1')
                else raw_num
            )
            try:
                formatted_number: str = phonenumbers.format_number(
                    phonenumbers.parse(full_phone_number),
                    PhoneNumberFormat.INTERNATIONAL,
                )
                return formatted_number
            except NumberParseException as npe:
                print(
                    f"ERROR: Could not parse '{num}' or '{full_phone_number}' as PhoneNumber: {npe}"
                )
                return None

        phone_numbers: List[PhoneNumber] = []
        phone1_nums = row.get(self.keys.PHONE_1, '').split(',')
        phone2_nums = row.get(self.keys.PHONE_2, '').split(',')
        phone3_nums = row.get(self.keys.PHONE_3, '').split(',')
        all_nums = {
            'Phone 1': phone1_nums,
            'Phone 2': phone2_nums,
            'Phone 3': phone3_nums,
        }

        for key, nums in all_nums.items():
            for n in nums:
                num = format_phone(n)
                if num and not any(p.number == num for p in phone_numbers):
                    num = PhoneNumber(number=num, name=key)
                    phone_numbers.append(num)

        return phone_numbers

    def extract_email_addresses(self, row: Row) -> List[PhoneNumber]:
        additional_emails: List[AdditionalEmailAddress] = []
        email2_addr = row.get(self.keys.EMAIL_2, '').strip()
        email3_addr = row.get(self.keys.EMAIL_3, '').strip()
        email4_addr = row.get(self.keys.EMAIL_4, '').strip()
        all_addrs = {
            'Email 2': email2_addr,
            'Email 3': email3_addr,
            'Email 4': email4_addr,
        }

        for key, addr in all_addrs.items():
            if addr and not any(e.email_address == addr for e in additional_emails):
                addl_email = AdditionalEmailAddress(name=key, email_address=addr)
                additional_emails.append(addl_email)

        return additional_emails

    def extract_member(self, row: Row) -> Member:
        email = row.get(self.keys.EMAIL_1, '').strip() or None
        normalized_email = Member.normalize_email(email) if email else None

        first_name = row.get(self.keys.FIRST_NAME, '').strip()
        preferred_name = row.get(self.keys.PREFERRED_NAME, '').strip()
        do_not_call = self.parse_do_not_call(
            row.get(self.keys.COMMS_PREFERENCE, '').strip()
        )
        do_not_email = self.parse_do_not_email(
            row.get(self.keys.DO_NOT_EMAIL, '').strip(),
            row.get(self.keys.COMMS_PREFERENCE, '').strip(),
        )
        notes = row.get(self.keys.NOTES, '').strip()

        city = row.get(self.keys.CITY, '').strip()
        zipcode = row.get(self.keys.ZIP, '').strip()

        return Member(
            first_name=preferred_name or first_name or None,
            last_name=row.get(self.keys.LAST_NAME, '').strip() or None,
            email_address=email,
            normalized_email=normalized_email,
            do_not_call=do_not_call,
            do_not_email=do_not_email,
            notes=notes,
            city=city,
            zipcode=zipcode,
        )

    def parse_do_not_call(self, raw_str: str) -> bool:
        if len(raw_str) == 0:
            return False

        lowered = raw_str.lower()
        if (
            lowered.startswith('moved')
            or lowered.startswith('disconnected')
            or lowered.startswith('no calls')
            or lowered.startswith('remove')
            or 'wrong number' in lowered
        ):
            return True

        return False

    def parse_do_not_email(self, mailchimp_raw: str, comms_raw: str) -> bool:
        if len(comms_raw) == 0 and len(mailchimp_raw) == 0:
            return False

        comms_lower = comms_raw.lower()
        mailchimp_lower = mailchimp_raw.lower()
        if (
            'unsubscribed' in mailchimp_lower
            or 'no email' in comms_lower
            or 'msg delivery fail' in comms_lower
            or 'message not delivered' in comms_lower
        ):
            return True

        return False

    def exclude_row(self, row: Row) -> bool:
        comms_raw = row.get(self.keys.COMMS_PREFERENCE, '').strip()
        return comms_raw.lower().startswith('remove')

    def extract_identities(self, row: Row) -> List[Identity]:
        ids: List[Identity] = []
        sv_dsa_id = row.get(self.keys.SV_DSA_ID, '').strip()
        if sv_dsa_id:
            identity = Identity(
                provider_name=IdentityProviders.CSV, provider_id=sv_dsa_id
            )
            ids.append(identity)
        return ids

    # def extract_membership(self, row: Row) -> NationalMembershipData:
    #     def format_optional_date(dts: Optional[str]) -> Optional[datetime]:
    #         if not dts:
    #             return None
    #         try:
    #             # Format 1 (March 2018)
    #             dt = datetime.strptime(dts, '%m/%d/%Y')
    #         except ValueError:
    #             # Format 2 (May 2018)
    #             dt = datetime.strptime(dts, '%m/%d/%y %H:%M')
    #         return dt.astimezone(CHAPTER_TIME_ZONE)

    #     membership = NationalMembershipData()
    #     # Add or updated membership data
    #     membership.ak_id = row[self.keys.AK_ID]  # required
    #     membership.dsa_id = row.get(self.keys.DSA_ID, '').strip() or None
    #     membership.first_name = row[self.keys.FIRST_NAME]  # required
    #     membership.middle_name = row.get(self.keys.MIDDLE_NAME, '').strip() or None
    #     membership.last_name = row[self.keys.LAST_NAME]  # required
    #     membership.active = row.get(self.keys.ACTIVE) == 'M'
    #     membership.address_line_1 = row.get(self.keys.ADDRESS_1, '').strip() or None
    #     membership.address_line_2 = row.get(self.keys.ADDRESS_2, '').strip() or None
    #     membership.city = row.get(self.keys.CITY, '').strip() or None
    #     membership.zipcode = row.get(self.keys.ZIP, '').strip() or None
    #     if membership.zipcode:
    #         raw_zip = membership.zipcode.replace('-', '')
    #         raw_zip_len = len(raw_zip)
    #         if not raw_zip.isdigit() or raw_zip_len != 5 and raw_zip_len != 9:
    #             raise ValueError("Invalid zipcode format: '{}'".format(raw_zip))
    #         elif raw_zip_len == 9:
    #             membership.zipcode = raw_zip[:5] + '-' + raw_zip[5:]
    #         else:
    #             membership.zipcode = raw_zip
    #     membership.country = row.get(self.keys.COUNTRY, '').strip() or None
    #     membership.do_not_call = row.get(self.keys.DO_NOT_CALL) == 'TRUE'
    #     membership.join_date = format_optional_date(row.get(self.keys.JOIN_DATE))
    #     membership.dues_paid_until = format_optional_date(row.get(self.keys.EXP_DATE))
    #     return membership

    TConfirmValue = TypeVar('TConfirmValue')

    def import_all(
        self,
        row_iter: Iterator[Dict[str, str]],
        limit: Optional[int] = None,
        out: IO[str] = sys.stdout,
        confirm_action: Callable[
            [TConfirmValue, str], ConfirmAction[TConfirmValue]
        ] = ConfirmAction,
    ) -> Results:
        """
        New members:
        - create member row
        - give member role

        Existing members:
        - don't update member row
        - give member role

        Membership:
        - attach to updated or created member by id
        - update from csv
        """

        # Confirm the updates for applying them
        def confirm(value: int, msg: str) -> ConfirmAction[int]:
            confirm_action: Callable[[int, str], ConfirmAction[int]] = ConfirmAction[
                int
            ]
            return confirm_action(value, f"{msg} (affects {value})")

        def confirm_bool(msg: str) -> ConfirmAction[bool]:
            confirm_action: Callable[[bool, str], ConfirmAction[bool]] = ConfirmAction[
                bool
            ]
            return confirm_action(True, msg)

        def resolve_name_duplicates(
            members: List[Tuple[Row, Member, bool]]
        ) -> List[Tuple[Row, Member, bool]]:
            print("Checking for members with the same name")
            name_duplicate_csv_entries: DefaultDict[
                Tuple[str, str], List[Tuple[Row, Member, int]]
            ] = defaultdict(list)
            for idx, (row, member, is_new) in enumerate(members):
                if is_new and member.first_name and member.last_name:
                    name_duplicate_csv_entries[
                        (member.first_name, member.last_name)
                    ].append((row, member, idx))

            name_duplicate_set = set(
                entry
                for entry in name_duplicate_csv_entries
                if len(name_duplicate_csv_entries[entry]) > 1
            )

            if len(name_duplicate_set) > 0:
                print("Found more than one member with the same name")
                print(
                    "This might be normal (some people just have the same name), or it may indicate"
                )
                print("a problem with the incoming CSV file.")
                print("\nYou MUST take action to continue importing this CSV.")
                with confirm(
                    len(name_duplicate_set), 'resolve name duplicates'
                ) as confirmed:
                    if confirmed is None:
                        raise ValueError(
                            "Cannot import without resolving name duplicates"
                        )

                    exclude_idxs = set()
                    for entry in name_duplicate_set:
                        entries = name_duplicate_csv_entries[entry]
                        print(f"\n\n=======\nName: {entry[0]} {entry[1]}")
                        print(f"contains {len(entries)} rows:")
                        print('-------')
                        print('preview of rows')
                        for row, member, i in entries:
                            print()
                            pprint(self._debug_row(row))
                        print('-------')
                        print('resolving duplicates now')
                        for row, member, i in entries:
                            print()
                            pprint(self._debug_row(row))
                            resp = input("Accept this row for this name? ([Y]es/[N]o) ")
                            if resp.lower().startswith('n'):
                                exclude_idxs.add(i)

                    return [
                        entry
                        for i, entry in enumerate(members)
                        if i not in exclude_idxs
                    ]
            else:
                return members

        def resolve_email_duplicates(
            members: List[Tuple[Row, Member, bool]]
        ) -> List[Tuple[Row, Member, bool]]:
            print("Checking for members with the same email")
            email_duplicate_csv_entries: DefaultDict[
                str, List[Tuple[Row, Member, int]]
            ] = defaultdict(list)
            for idx, (row, member, is_new) in enumerate(members):
                if is_new and member.email_address:
                    email_duplicate_csv_entries[member.email_address].append(
                        (row, member, idx)
                    )

            email_duplicate_set = set(
                entry
                for entry in email_duplicate_csv_entries
                if len(email_duplicate_csv_entries[entry]) > 1
            )

            if len(email_duplicate_set) > 0:
                print("WARNING: One email address used for multiple members!")
                print(
                    "Email addresses must be uniquely associated to a single member at this time"
                )
                print("\nYou MUST take action to continue importing this CSV.")
                with confirm(
                    len(email_duplicate_set), 'resolve email duplicates'
                ) as confirmed:
                    if confirmed is None:
                        raise ValueError(
                            "Cannot import without resolving email duplicates"
                        )

                    exclude_idxs: Set[int] = set()
                    for entry in email_duplicate_set:
                        entries = email_duplicate_csv_entries[entry]
                        print(f"\n\n=======\nEmail: {entry}")
                        print(f"contains {len(entries)} rows:")
                        print('-------')
                        print('preview of rows')
                        idx_choices = set()
                        for row, member, i in entries:
                            print(f"\nRow {i}:")
                            pprint(self._debug_row(row))
                            idx_choices.add(i)
                        print('-------')
                        resp = None
                        while resp not in idx_choices:
                            resp = int(input(f"Choose a row to keep ({idx_choices}) "))
                        if resp is not None:
                            idx_choices.remove(resp)
                        exclude_idxs |= idx_choices

                    return [
                        entry
                        for i, entry in enumerate(members)
                        if i not in exclude_idxs
                    ]
            else:
                return members

        def resolve_members(
            new_members: List[Tuple[Row, Member, bool]]
        ) -> List[Tuple[Row, Member, bool]]:
            name_resolved_members = resolve_name_duplicates(new_members)
            email_resolved_members = resolve_email_duplicates(name_resolved_members)
            return email_resolved_members

        results = ImportMasterList.Results()
        if limit is None:
            row_iter, line_counter = itertools.tee(row_iter, 2)
            results.total = sum(1 for _ in line_counter)
        else:
            results.total = limit
        rows = list(itertools.islice(row_iter, 0, results.total))
        session = Session()

        try:
            # The row, and a list of all matching members or guests
            # for that row alongside a bool for whether they have the 'member' role.
            row_member_tuples: List[Tuple[Row, List[Tuple[Member, bool]]]] = [
                (
                    row,
                    # self.extract_membership(row),
                    self.find_existing_members(row, session),
                )
                for row in rows
                if not self.exclude_row(row)
            ]
            results.excluded = results.total - len(row_member_tuples)
            print(f'Row member tuples #: {len(row_member_tuples)}')

            # A list of row to ambiguous members tuples.
            # If any of the lists are not empty, then the members in the list are ambiguous.
            ambiguous_members: List[Tuple[Row, List[Tuple[Member, bool]]]] = [
                (row, members)
                for row, members in row_member_tuples
                if len([member for member, has_role in members if has_role]) > 1
            ]

            # Validate against ambiguous memberships
            if len(ambiguous_members) > 0:
                raise ValueError(
                    f"Found multiple members with the same email or first_name and last_name in"
                    f" the following rows:\n"
                    + '\n \n'.join(
                        "Row: {}\nMatches:\n\t{}".format(
                            self._debug_row(row),
                            '\n\t'.join(
                                [
                                    str(self._debug_member(member))
                                    for member, has_role in members
                                ]
                            ),
                        )
                        for row, members in ambiguous_members
                    )
                    + "\n \n"  # NOQA: W291 weird bug with trailing whitespace detection
                    "NOTE: Avoid this error by pre-pending 'DELETE_' to the"
                    " 'members.email_addresses' field"
                )

            ambiguous_guests = [
                (row, members)
                for row, members in row_member_tuples
                if not any(has_role for _, has_role in members) and len(members) > 1
            ]

            if len(ambiguous_guests) > 0:
                raise ValueError(
                    f"Found multiple guests matching the same email or first_name and last_name in"
                    f" the following rows:\n"
                    + '\n \n'.join(
                        "Row: {}\nMatches:\n\t{}".format(
                            self._debug_row(row),
                            '\n\t'.join(
                                [
                                    str(self._debug_member(member))
                                    for member, has_role in members
                                ]
                            ),
                        )
                        for row, members in ambiguous_guests
                    )
                    + "\n \n"  # NOQA: W291 weird bug with trailing whitespace detection
                    "NOTE: Avoid this error by pre-pending 'DELETE_' to the"
                    " 'members.email_addresses' field"
                )

            ##
            # Phase 1: Create and update the members
            ##

            # If the member already update, else create from the row
            existing_members: List[Tuple[Row, Optional[Member]]] = [
                (
                    row,
                    (
                        # find the unambiguous member (as verified above):
                        next(iter(m for m, has_role in members if has_role), None)
                        or
                        # or else the first unambiguous guest that matches (as verified above):
                        next(iter(m for m, _ in members), None)
                    ),
                )
                for row, members in row_member_tuples
            ]
            print(
                f'''Existing members #: {len([
                    member
                    for member in existing_members
                    if member[1] is not None
                ])}'''
            )
            # print(f'Existing members[0]: {existing_members[0]}')

            new_members: List[Tuple[Row, Member, bool]] = [
                (
                    row,
                    existing_member if existing_member else self.extract_member(row),
                    existing_member is None,
                )
                for row, existing_member in existing_members
            ]
            print(f'New members #: {len(new_members)}')

            resolved_members: List[Tuple[Row, Member, bool]] = resolve_members(
                new_members
            )

            members_to_add: Dict[str, Member] = {
                member.email_address: member
                for row, member, is_new in resolved_members
                if is_new and member.email_address is not None
            }
            members_with_no_email: List[Member] = [
                member
                for row, member, is_new in resolved_members
                if is_new and member.email_address is None
            ]
            print(
                f'Members to add #: '
                f'{len(members_to_add) + len(members_with_no_email)} total = '
                f'{len(members_to_add)} with emails + {len(members_with_no_email)} without emails'
            )

            # print("\n\n".join([
            #     str(self._debug_row(row))
            #     for row, member, is_new in resolved_members
            #     if is_new and member.email_address is None
            # ]))

            with confirm(len(members_to_add), 'add members with emails') as confirmed:
                if confirmed is None:
                    return results
                if members_to_add:
                    for email, member in members_to_add.items():
                        print(
                            f"adding member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        session.add(member)
                    session.commit()
                    results.members_created += confirmed
                    refresh_all(members_to_add, session)

            with confirm(
                len(members_with_no_email), 'add members with no email'
            ) as confirmed:
                if confirmed is None:
                    return results
                if members_with_no_email:
                    for member in members_with_no_email:
                        print(
                            f"adding member with no email: {member.name} ({member.phone_numbers})",
                            file=out,
                        )
                        session.add(member)
                    session.commit()
                    results.members_created += confirmed
                    refresh_all(members_to_add, session)

            ##
            # Phase 2:
            # Add all phone numbers, emails, and identities
            ##

            identities_to_add: List[Tuple[Member, Identity]] = [
                (member, identity)
                for row, member, _ in resolved_members
                for identity in self.extract_identities(row)
            ]

            # Add all identities from memberships
            with confirm(len(identities_to_add), 'identities to add') as confirmed:
                if confirmed is None:
                    return results
                if identities_to_add:
                    identities: Dict[str, Identity] = {}
                    for member, identity in identities_to_add:
                        identity.member_id = member.id
                        print(
                            f"adding identity ({identity.provider_name}={identity.provider_id}) "
                            f"to member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        identities[
                            f"{identity.provider_name}:{identity.provider_id}"
                        ] = identity
                    try:
                        self.identities.add_all(session, list(identities.values()))
                        results.identities_created += len(identities)
                    except IntegrityError:
                        print('rolled back identities.')
                        session.rollback()
                    # no need to refresh

            phone_numbers_to_add: List[Tuple[Member, PhoneNumber]] = [
                (member, phone_number)
                for row, member, _ in resolved_members
                for phone_number in self.extract_phone_numbers(row)
            ]

            with confirm(
                len(phone_numbers_to_add), 'phone_numbers to add'
            ) as confirmed:
                if confirmed is None:
                    return results
                if phone_numbers_to_add:
                    phone_numbers: Dict[str, PhoneNumber] = {}
                    for member, phone_number in phone_numbers_to_add:
                        phone_number.member_id = member.id
                        print(
                            f"adding phone number ({phone_number.name}={phone_number.number}) "
                            f"to member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        phone_numbers[
                            f"{phone_number.name}:{phone_number.number}"
                        ] = phone_number
                    try:
                        self.phone_numbers.add_all(
                            session, list(phone_numbers.values())
                        )
                        results.phone_numbers_created += len(phone_numbers)
                    except IntegrityError:
                        print('rolled back phone numbers.')
                        session.rollback()
                    # no need to refresh

            email_addresses_to_add: List[Tuple[Member, AdditionalEmailAddress]] = [
                (member, email_address)
                for row, member, _ in resolved_members
                for email_address in self.extract_email_addresses(row)
            ]

            with confirm(
                len(email_addresses_to_add), 'email_addresses to add'
            ) as confirmed:
                if confirmed is None:
                    return results
                if email_addresses_to_add:
                    email_addresses: Dict[str, AdditionalEmailAddress] = {}
                    for member, email_address in email_addresses_to_add:
                        email_address.member_id = member.id
                        print(
                            f"adding email address "
                            f"({email_address.name}={email_address.email_address}) "
                            f"to member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        email_addresses[
                            f"{email_address.name}:{email_address.email_address}"
                        ] = email_address
                    try:
                        self.addl_email_addresses.add_all(
                            session, list(email_addresses.values())
                        )
                        results.email_addresses_created += len(email_addresses)
                    except IntegrityError:
                        print('rolled back phone numbers.')
                        session.rollback()
                    # no need to refresh

                    ##
                    # Phase 3:
                    # Create branch committees and roles
                    ##
            branches: DefaultDict[str, List[Member]] = defaultdict(list)
            for row, member, _ in resolved_members:
                branches[row.get(self.keys.BRANCH, '').strip()].append(member)
            print(f"Branches seen: {branches.keys()}")

            filtered_branches = {
                branch: members
                for branch, members in branches.items()
                if branch in BRANCHES
            }
            print(f"Filtered branches seen: {filtered_branches.keys()}")

            with confirm(len(filtered_branches), 'branches to add') as confirmed:
                if confirmed is None:
                    return results

                try:
                    for branch, members in filtered_branches.items():
                        existing_committees = (
                            session.query(Committee)
                            .filter(Committee.name == branch)
                            .all()
                        )

                        if existing_committees is None or len(existing_committees) == 0:
                            print(f"Adding branch {branch}")
                            branch_committee = Committee(name=branch)
                            session.add(branch_committee)
                        elif len(existing_committees) == 1:
                            print(f"Found existing branch {branch}")
                            branch_committee = existing_committees[0]

                        for member in members:
                            new_role = Role(
                                role='member',
                                committee=branch_committee,
                                member=member,
                            )
                            print(
                                f"Adding role for branch {branch} for "
                                f"member: {member.name} ({member.email_address})"
                            )
                            session.add(new_role)
                    session.commit()
                    results.member_roles_added += len(
                        [members for branch, members in filtered_branches.items()]
                    )
                except IntegrityError as e:
                    print("Rolled back committees and roles", e)
                    session.rollback()

            # Return the completed results
            return results

        finally:
            session.close()
