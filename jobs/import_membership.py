#!/usr/bin/env python3

import argparse
import csv
import io  # NOQA: F401
import itertools
import re
import sys
from datetime import datetime
from typing import (
    IO,
    Any,
    Callable,
    Dict,
    Iterable,
    Iterator,
    List,
    Optional,
    Tuple,
)

import phonenumbers
from overrides import overrides
from phonenumbers import NumberParseException, PhoneNumberFormat
from sqlalchemy import and_, func, or_
from sqlalchemy.exc import IntegrityError, InvalidRequestError, SQLAlchemyError

from jobs import ConfirmAction, ForceConfirmAction, Job, V
from membership.database.base import Base, Session, col
from membership.database.models import (
    AdditionalEmailAddress,
    Identity,
    IdentityProviders,
    Member,
    NationalMembershipData,
    PhoneNumber,
    Role,
)
from membership.services import PhoneNumberService
from membership.services.national import IdentityService
from membership.util.time import CHAPTER_TIME_ZONE


class NationalMembershipKeys:
    AK_ID = "AK_ID"
    DSA_ID = "DSA_ID"
    EMAIL = "Email"
    ACTIVE = "Memb_status"
    JOIN_DATE = "Join_Date"
    EXP_DATE = "Xdate"
    FIRST_NAME = "first_name"
    MIDDLE_NAME = "middle_name"
    LAST_NAME = "last_name"
    ZIP = "Zip"
    ADDRESS_1 = "Address_Line_1"
    ADDRESS_2 = "Address_Line_2"
    CITY = "City"
    STATE = "State"
    COUNTRY = "Country"
    HOME_PHONE = "Home_Phone"
    MOBILE_PHONE = "Mobile_Phone"
    WORK_PHONE = "Work_Phone"
    DO_NOT_CALL = "Do_Not_Call"


# typedef
Row = Dict[str, str]


def fields_of(table: Base, model, excluding: Iterable[str] = ("id",)) -> dict:
    exclude_set = set(excluding)
    return {
        column: getattr(model, column)
        for column in table.__table__.columns.keys()
        if column not in exclude_set
    }


def refresh_all(objects: Iterable[object], session: Session) -> None:
    for obj in objects:
        try:
            session.refresh(obj)
        except InvalidRequestError:
            pass


class ImportNationalMembership(Job):
    """
    Imports the national membership list without sending any emails / invites on auth0.

    Gracefully handles:

    1. Existing Member:
        Inserts or updates existing Membership without touching Member
        Adds missing identities
        Adds member role if missing

    2. No Existing Member:
        Inserts or updates Membership
        Inserts Member with
        Adds identities
        Adds member role

    Matches existing member on normalized email or first_name + last_name
    """

    description = "Upload national membership roll"
    keys = NationalMembershipKeys
    identities = IdentityService()
    phone_numbers = PhoneNumberService()

    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            "input",
            nargs="?",
            type=argparse.FileType("r", encoding="utf_8"),
            default=sys.stdin,
        )
        parser.add_argument(
            "-o", "--output", type=argparse.FileType("w"), default=sys.stdout
        )
        parser.add_argument("-f", "--force", action="store_true")

    @overrides
    def run(self, config: dict) -> None:
        outfile: IO[str] = config.get("output", sys.stdout)
        row_iter: Optional[io.BufferedReader] = config.get("input")
        if row_iter is None:
            raise ValueError(
                f"config['input'] must be an iterator over a CSV file, received None "
                f"(provided keys: {config.keys()})"
            )
        reader = csv.DictReader(row_iter)
        results = self.import_all(
            iter(reader),
            out=outfile,
            confirm_action=ForceConfirmAction if config.get("force") else ConfirmAction,
        )
        print(f"\n{results}", file=outfile)

    class Results:
        def __init__(self, src: Dict[str, Any] = {}, **kwargs):
            results = src or kwargs
            self.members_created: int = results.get("members_created", 0)
            self.members_updated: int = results.get("members_updated", 0)
            self.memberships_created: int = results.get("memberships_created", 0)
            self.memberships_updated: int = results.get("memberships_updated", 0)
            self.member_roles_added: int = results.get("member_roles_added", 0)
            self.identities_created: int = results.get("identities_created", 0)
            self.phone_numbers_created: int = results.get("phone_numbers_created", 0)
            self.errors: int = results.get("errors", 0)
            self.processed: int = results.get("processed", 0)
            self.total: int = results.get("total", 0)

        def __str__(self):
            return (
                f"Members Created: {self.members_created}\n"
                f"Members Updated: {self.members_updated}\n"
                f"Memberships Created: {self.memberships_created}\n"
                f"Memberships Updated: {self.memberships_updated}\n"
                f"Member Roles Added: {self.member_roles_added}\n"
                f"Identities Created: {self.identities_created}\n"
                f"Phone Numbers Created: {self.phone_numbers_created}\n"
                f"Errors: {self.errors}\n"
                f"Processed: {self.processed}\n"
                f"Total: {self.total}\n"
            )

        def __repr__(self):
            return f"ImportNationalMembership.Results({repr(vars(self))})"

        def __copy__(self):
            newone = type(self)()
            newone.__dict__.update(self.__dict__)
            return newone

        def __deepcopy__(self, memodict={}):
            return self.__copy__()

    def _debug_row(self, row: Dict[str, str]) -> Dict[str, str]:
        return {
            k: v
            for k, v in row.items()
            if k
            in {
                self.keys.AK_ID,
                self.keys.FIRST_NAME,
                self.keys.LAST_NAME,
                self.keys.EMAIL,
            }
        }

    def _debug_member(self, member: Member) -> Dict[str, str]:
        return {
            "first_name": member.first_name,
            "last_name": member.last_name,
            "email_address": member.email_address,
        }

    def find_existing_members(
        self, row: Row, session: Session, out: IO[str]
    ) -> List[Tuple[Member, bool]]:
        """
        Attempts to find an existing member using the given normalized_email, first_name, and
        last_name.

        :return: List of matching member objects and whether these members have the 'member' role
        """
        try:
            email = row.get(self.keys.EMAIL, "").strip()
            first_name = row.get(self.keys.FIRST_NAME, "").strip()
            last_name = row.get(self.keys.LAST_NAME, "").strip()
            clauses = []
            if email:
                normalized_email = Member.normalize_email(email)
                clauses.append(
                    or_(
                        Member.normalized_email == normalized_email,
                        func.lower(Member.email_address) == email.lower(),
                    )
                )
            if first_name and last_name:
                clauses.append(
                    and_(
                        func.lower(Member.first_name) == first_name.lower(),
                        func.lower(Member.last_name) == last_name.lower(),
                    )
                )
            # this is necessary because if we are given no criteria, we should return an
            # empty list instead of every member in the database.
            if not clauses:
                return []
            found = session.query(Member).filter(or_(*clauses)).all()
            addl_emails = (
                session.query(AdditionalEmailAddress)
                .filter(AdditionalEmailAddress.email_address == email.lower())
                .all()
            )
            found_ids = set(m.id for m in found)
            email_members = [
                email_address.member
                for email_address in addl_emails
                if email_address.member_id not in found_ids
            ]
            return [
                (
                    m,
                    any(
                        r
                        for r in m.roles
                        if r.role == "member" and r.committee_id is None
                    ),
                )
                for m in found + email_members
                if (not m.email_address or not m.email_address.startswith("DELETE"))
            ]
        except SQLAlchemyError as e:
            print(f"ERROR: {e}", file=out)
            return []

    def extract_phone_numbers(self, row: Dict[str, str]) -> List[PhoneNumber]:
        # TODO: Replace with a call to PhoneNumberService
        def format_phone(num: str) -> Optional[str]:
            raw_num = re.sub("[(). +-]*", lambda m: "", num)
            if not raw_num:
                return None
            full_phone_number = (
                f"+1 {num}"
                if len(raw_num) == 10 or (len(raw_num) == 11 and raw_num[0] == "1")
                else raw_num
            )
            try:
                return phonenumbers.format_number(
                    phonenumbers.parse(full_phone_number),
                    PhoneNumberFormat.INTERNATIONAL,
                )
            except NumberParseException as npe:
                print(
                    f"ERROR: Could not parse '{num}' or '{full_phone_number}' as PhoneNumber: {npe}"
                )
                return None

        phone_numbers: List[PhoneNumber] = []
        home_nums = row.get(self.keys.HOME_PHONE, "").split(",")
        cell_nums = row.get(self.keys.MOBILE_PHONE, "").split(",")
        work_nums = row.get(self.keys.WORK_PHONE, "").split(",")
        for n in cell_nums:
            cell_num = format_phone(n)
            if cell_num and not any(p.number == cell_num for p in phone_numbers):
                num = PhoneNumber(number=cell_num, name="Cell")
                phone_numbers.append(num)
        for n in home_nums:
            home_num = format_phone(n)
            if home_num and not any(p.number == home_num for p in phone_numbers):
                num = PhoneNumber(number=home_num, name="Home")
                phone_numbers.append(num)
        for n in work_nums:
            work_num = format_phone(n)
            if work_num and not any(p.number == work_num for p in phone_numbers):
                num = PhoneNumber(number=work_num, name="Work")
                phone_numbers.append(num)
        return phone_numbers

    def extract_member(self, row: Row) -> Member:
        email = row.get(self.keys.EMAIL, "").strip() or None
        normalized_email = Member.normalize_email(email) if email else None
        return Member(
            first_name=row.get(self.keys.FIRST_NAME, "").strip() or None,
            last_name=row.get(self.keys.LAST_NAME, "").strip() or None,
            email_address=email,
            normalized_email=normalized_email,
        )

    def extract_identities(self, row: Row) -> List[Identity]:
        ids: List[Identity] = []
        ak_id = row.get(self.keys.AK_ID, "").strip()
        if ak_id:
            identity = Identity(provider_name=IdentityProviders.AK, provider_id=ak_id)
            ids.append(identity)
        dsa_id = row.get(self.keys.DSA_ID, "").strip()
        if dsa_id:
            identity = Identity(provider_name=IdentityProviders.DSA, provider_id=dsa_id)
            ids.append(identity)
        return ids

    def extract_membership(self, row: Row) -> NationalMembershipData:
        def format_optional_date(dts: Optional[str]) -> Optional[datetime]:
            if not dts:
                return None
            try:
                # Date Format 2 (Feb 2020)
                dt = datetime.strptime(dts, "%Y-%m-%d")
            except ValueError:
                try:
                    # Date Time Format 2 (Feb 2020)
                    dt = datetime.strptime(dts, "%Y-%m-%d %H:%M")
                except ValueError:
                    try:
                        # Date Format 1 (May 2018)
                        dt = datetime.strptime(dts, "%m/%d/%y %H:%M")
                    except ValueError:
                        # Date Time Format 1 (March 2018)
                        dt = datetime.strptime(dts, "%m/%d/%Y")
            return dt.astimezone(CHAPTER_TIME_ZONE)

        membership = NationalMembershipData()
        # Add or updated membership data
        membership.ak_id = row[self.keys.AK_ID]  # required
        membership.dsa_id = row.get(self.keys.DSA_ID, "").strip() or None
        membership.first_name = row[self.keys.FIRST_NAME]  # required
        membership.middle_name = row.get(self.keys.MIDDLE_NAME, "").strip() or None
        membership.last_name = row[self.keys.LAST_NAME]  # required
        membership.active = row.get(self.keys.ACTIVE) == "M"
        membership.address_line_1 = row.get(self.keys.ADDRESS_1, "").strip() or None
        membership.address_line_2 = row.get(self.keys.ADDRESS_2, "").strip() or None
        membership.city = row.get(self.keys.CITY, "").strip() or None
        membership.zipcode = row.get(self.keys.ZIP, "").strip() or None
        if membership.zipcode:
            raw_zip = membership.zipcode.replace("-", "")
            raw_zip_len = len(raw_zip)
            if not raw_zip.isdigit() or raw_zip_len != 5 and raw_zip_len != 9:
                raise ValueError("Invalid zipcode format: '{}'".format(raw_zip))
            elif raw_zip_len == 9:
                membership.zipcode = raw_zip[:5] + "-" + raw_zip[5:]
            else:
                membership.zipcode = raw_zip
        membership.country = row.get(self.keys.COUNTRY, "").strip() or None
        membership.do_not_call = row.get(self.keys.DO_NOT_CALL) == "TRUE"
        membership.join_date = format_optional_date(row.get(self.keys.JOIN_DATE))
        membership.dues_paid_until = format_optional_date(row.get(self.keys.EXP_DATE))
        return membership

    def import_all(
        self,
        row_iter: Iterator[Dict[str, str]],
        limit: Optional[int] = None,
        out: IO[str] = sys.stdout,
        confirm_action: Callable[[V, str], ConfirmAction] = ConfirmAction,
    ) -> Results:
        """
        New members:
        - create member row
        - give member role

        Existing members:
        - don't update member row
        - give member role

        Membership:
        - attach to updated or created member by id
        - update from csv
        """

        results = ImportNationalMembership.Results()
        if limit is None:
            row_iter, line_counter = itertools.tee(row_iter, 2)
            results.total = sum(1 for _ in line_counter)
        else:
            results.total = limit
        rows = list(itertools.islice(row_iter, 0, results.total))
        session = Session()

        try:

            # The row, the extracted membership data, and a list of all matching members or guests
            # for that row alongside a bool for whether they have the 'member' role.
            membership_tuples: List[
                Tuple[Row, NationalMembershipData, List[Tuple[Member, bool]]]
            ] = [
                (
                    row,
                    self.extract_membership(row),
                    self.find_existing_members(row, session, out),
                )
                for row in rows
            ]

            def find_matching_member_or_extract(
                row: Row,
                membership: NationalMembershipData,
                members: List[Tuple[Member, bool]],
            ) -> Tuple[Row, NationalMembershipData, Member, bool]:
                existing_member = (
                    # find the unambiguous member (as verified above):
                    next(iter(m for m, has_role in members if has_role), None)
                    or
                    # or else the first unambiguous guest that matches (as verified above):
                    next(iter(m for m, _ in members), None)
                )
                matching_member = existing_member or self.extract_member(row)
                return (
                    row,
                    membership,
                    matching_member,
                    not existing_member,
                )

            memberships_and_matching_members: List[
                Tuple[Row, NationalMembershipData, Member, bool]
            ] = [
                find_matching_member_or_extract(row, membership, members)
                for row, membership, members in membership_tuples
            ]

            # A list of all AK ids from the memberships in the file
            membership_ak_ids: List[str] = [
                membership.ak_id for row, membership, members in membership_tuples
            ]

            # A dict of AK id to membership data for all existing memberships
            existing_memberships_by_ak_id: Dict[str, NationalMembershipData] = {
                membership.ak_id: membership
                for membership in session.query(NationalMembershipData)
                .filter(col(NationalMembershipData.ak_id).in_(membership_ak_ids),)
                .all()
            }

            new_membership_tuples: List[
                Tuple[Row, NationalMembershipData, List[Tuple[Member, bool]]]
            ] = [
                (row, membership, members)
                for row, membership, members in membership_tuples
                if membership.ak_id not in existing_memberships_by_ak_id
            ]

            # A list of row to ambiguous members tuples.
            # If any of the lists are not empty, then the members in the list are ambiguous.
            ambiguous_members: List[Tuple[Row, List[Tuple[Member, bool]]]] = [
                (row, members)
                for row, membership, members in new_membership_tuples
                if len([member for member, has_role in members if has_role]) > 1
            ]

            # Step 2: Validate against ambiguous memberships

            if len(ambiguous_members) > 0:
                raise ValueError(
                    f"Found multiple members with the same email or first_name and last_name in"
                    f" the following rows:\n"
                    + "\n \n".join(
                        "Row: {}\nMatches:\n\t{}".format(
                            self._debug_row(row),
                            "\n\t".join(
                                [
                                    str(self._debug_member(member))
                                    for member, has_role in members
                                ]
                            ),
                        )
                        for row, members in ambiguous_members
                    )
                    + "\n \n"
                    "NOTE: Avoid this error by pre-pending 'DELETE_' to the"
                    " 'members.email_addresses' field"
                )

            ambiguous_guests = [
                (row, members)
                for row, membership, members in new_membership_tuples
                if not any(has_role for _, has_role in members) and len(members) > 1
            ]

            if len(ambiguous_guests) > 0:
                raise ValueError(
                    f"Found multiple guests matching the same email or first_name and last_name in"
                    f" the following rows:\n"
                    + "\n \n".join(
                        "Row: {}\nMatches:\n\t{}".format(
                            self._debug_row(row),
                            "\n\t".join(
                                [
                                    str(self._debug_member(member))
                                    for member, has_role in members
                                ]
                            ),
                        )
                        for row, members in ambiguous_guests
                    )
                    + "\n \n"
                    "NOTE: Avoid this error by pre-pending 'DELETE_' to the"
                    " 'members.email_addresses' field"
                )

            # Confirm the updates for applying them
            def confirm(value: int, msg: str) -> ConfirmAction[int]:
                return confirm_action(value, f"{msg} (affects {value})")

            ##
            # Phase 1: Create and update the members
            ##

            # If the member already update, else create from the row

            new_memberships_and_members: List[
                Tuple[Row, NationalMembershipData, Member, bool]
            ] = [
                (row, membership, member, is_new)
                for row, membership, member, is_new in memberships_and_matching_members
                if membership.ak_id not in existing_memberships_by_ak_id
            ]
            members_to_add: List[Member] = [
                member
                for row, _, member, is_new in new_memberships_and_members
                if is_new
            ]
            with confirm(len(members_to_add), "members to add") as confirmed:
                if confirmed is None:
                    return results
                if members_to_add:
                    for member in members_to_add:
                        print(
                            f"adding member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        session.add(member)
                    session.commit()
                    results.members_created += confirmed
                    refresh_all(members_to_add, session)

            # Update all members marked as do not call
            do_not_call_memberships_and_existing_members: List[
                Tuple[NationalMembershipData, Member]
            ] = [
                (membership, member)
                for _, membership, members in membership_tuples
                for member, _ in members
                if membership.do_not_call
                and membership.ak_id in existing_memberships_by_ak_id
            ]
            for membership, member in do_not_call_memberships_and_existing_members:
                member.do_not_call = member.do_not_call or membership.do_not_call
            members_to_update: List[Member] = [
                member for _, member in do_not_call_memberships_and_existing_members
            ]
            with confirm(len(members_to_update), "members to update") as confirmed:
                if confirmed is None:
                    return results
                if members_to_update:
                    for member in members_to_update:
                        print(f"updating member: {member.name}", file=out)
                        session.add(member)
                    session.commit()
                    results.members_updated += confirmed
                    refresh_all(members_to_update, session)

            ##
            # Phase 2: Create and update the memberships
            ##

            # Add all memberships found in the file and link them to the closest member

            memberships_to_add: List[NationalMembershipData] = []
            for row, membership, member, _ in new_memberships_and_members:
                membership.member_id = member.id
                memberships_to_add.append(membership)

            with confirm(len(memberships_to_add), "memberships to add") as confirmed:
                if confirmed is None:
                    return results
                if memberships_to_add:
                    for membership in memberships_to_add:
                        print(
                            f"adding membership: {membership.first_name} {membership.last_name}",
                            file=out,
                        )
                        session.add(membership)
                    try:
                        session.commit()
                        results.memberships_created += confirmed
                    except IntegrityError:
                        session.rollback()
                    refresh_all(memberships_to_add, session)

            # Update all memberships found in the file that match existing memberships

            memberships_to_update: List[NationalMembershipData] = []
            for row, membership, _ in membership_tuples:
                existing_membership = existing_memberships_by_ak_id.get(
                    membership.ak_id
                )
                if existing_membership:
                    update_props = fields_of(
                        NationalMembershipData,
                        membership,
                        excluding=("id", "member_id"),
                    )
                    for k, v in update_props.items():
                        setattr(existing_membership, k, v)
                    memberships_to_update.append(existing_membership)

            with confirm(
                len(memberships_to_update), "memberships to update"
            ) as confirmed:
                if confirmed is None:
                    return results
                if memberships_to_update:
                    for membership in memberships_to_update:
                        print(
                            f"updating membership: "
                            f"{membership.first_name} {membership.last_name}",
                            file=out,
                        )
                        session.add(membership)
                    session.commit()
                    results.memberships_updated += confirmed
                    refresh_all(memberships_to_update, session)

            ##
            # Phase 3: Add the member role to all members parsed from the file
            ##
            guest_lists: List[List[Member]] = [
                [m for m, has_role in members if not has_role]
                for _, _, members in membership_tuples
                if not any(
                    has_role for _, has_role in members
                )  # ignore existing members
            ]
            guests_to_upgrade: List[Member] = [
                non_members[0]
                for non_members in guest_lists
                if len(non_members) == 1  # ignore ambiguous guests
            ]

            roles_to_add: List[Tuple[Member, Role]] = [
                (guest, Role(role="member", committee_id=None, member_id=guest.id,))
                for guest in (*guests_to_upgrade, *members_to_add)
            ]
            with confirm(len(roles_to_add), "roles to add") as confirmed:
                if confirmed is None:
                    return results
                if roles_to_add:
                    for member, role in roles_to_add:
                        print(
                            f"adding role to member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        session.add(role)
                    session.commit()
                    results.member_roles_added += confirmed
                    # no need to refresh

            ##
            # Phase 4:
            # Add all phone numbers and identities
            ##

            identities_to_add: List[Tuple[Member, Identity]] = [
                (member, identity)
                for row, _, member, _ in memberships_and_matching_members
                for identity in self.extract_identities(row)
            ]

            # Add all identities from memberships
            with confirm(len(identities_to_add), "identities to add") as confirmed:
                if confirmed is None:
                    return results
                if identities_to_add:
                    identities: Dict[str, Identity] = {}
                    for member, identity in identities_to_add:
                        identity.member_id = member.id
                        print(
                            f"adding identity ({identity.provider_name}={identity.provider_id}) "
                            f"to member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        identities[
                            f"{identity.provider_name}:{identity.provider_id}"
                        ] = identity
                    try:
                        self.identities.add_all(session, list(identities.values()))
                        results.identities_created += len(identities)
                    except IntegrityError:
                        print("rolled back identities.", file=out)
                        session.rollback()
                    # no need to refresh

            phone_numbers_to_add: List[Tuple[Member, PhoneNumber]] = [
                (member, phone_number)
                for row, _, member, _ in memberships_and_matching_members
                for phone_number in self.extract_phone_numbers(row)
            ]

            with confirm(
                len(phone_numbers_to_add), "phone_numbers to add"
            ) as confirmed:
                if confirmed is None:
                    return results
                if phone_numbers_to_add:
                    phone_numbers: Dict[str, PhoneNumber] = {}
                    for member, phone_number in phone_numbers_to_add:
                        phone_number.member_id = member.id
                        print(
                            f"adding phone number ({phone_number.name}={phone_number.number}) "
                            f"to member: {member.name} ({member.email_address})",
                            file=out,
                        )
                        phone_numbers[
                            f"{phone_number.name}:{phone_number.number}"
                        ] = phone_number
                    try:
                        self.phone_numbers.add_all(
                            session, list(phone_numbers.values())
                        )
                        results.phone_numbers_created += len(phone_numbers)
                    except IntegrityError:
                        print("rolled back phone numbers.", file=out)
                        session.rollback()
                    # no need to refresh

            # Return the completed results
            return results

        finally:
            session.close()
